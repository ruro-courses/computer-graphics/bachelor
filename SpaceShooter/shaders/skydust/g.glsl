#version 460
layout(row_major) uniform;

// CONST
const int repeats = 1;
const int max_primitives = (2*repeats) * (2*repeats) * (2*repeats);

// IN
layout (points) in;
in VERT_GEOM { vec3 pos; } gin[];

// OUT
layout (line_strip, max_vertices = 2 * max_primitives) out;
out GEOM_FRAG { vec4 col; } gout;
out gl_PerVertex { vec4 gl_Position; };

// UNIFORM
uniform mat4 U_projMatrix;
uniform mat4 U_viewMatrix;
uniform mat4 U_traceMatrix;

uniform float U_dustScale = 64;
uniform float U_trailLength = 0.2;
uniform vec4 U_trailBegin = vec4(1, 1, 1, 1);
uniform vec4 U_trailEnd = vec4(1, 1, 1, 0);

// CODE
vec3 get_local(in mat4 M)
{
    const vec3 p = U_dustScale + vec3(M[0].w, M[1].w, M[2].w) * inverse(mat3(M));
    return p - mod(p, 2*U_dustScale);
}

void render_block(in vec4 p, in ivec3 offset, in mat4 tpMat)
{
    p.xyz -= U_dustScale * offset;
    p *= U_viewMatrix;
    if (length(p) > repeats * U_dustScale) return;

    gout.col = U_trailBegin;
    gl_Position = p * U_projMatrix;
    EmitVertex();

    gout.col = U_trailEnd;
    gl_Position = p * tpMat;
    EmitVertex();

    EndPrimitive();
}

void main(void)
{
    vec4 dust = vec4(U_dustScale * gin[0].pos - get_local(U_viewMatrix), 1.0);
    mat4 tpMat = U_trailLength * U_traceMatrix * U_projMatrix;

    for (int i = -repeats; i <= repeats; ++i)
    for (int j = -repeats; j <= repeats; ++j)
    for (int k = -repeats; k <= repeats; ++k)
    render_block(dust, ivec3(i, j, k), tpMat);
}
