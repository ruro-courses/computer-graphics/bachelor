#version 460
layout(row_major) uniform;

// IN
in GEOM_FRAG { vec4 col; } fin;
// OUT
layout(location = 0) out vec4 col;
// CODE
void main(void) { col = fin.col; }
