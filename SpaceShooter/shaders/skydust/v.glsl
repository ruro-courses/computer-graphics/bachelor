#version 460
layout(row_major) uniform;

// IN
layout(location = 0) in vec3 pos;
// OUT
out VERT_GEOM { vec3 pos; } vout;
// CODE
void main(void) { vout.pos = pos; }
