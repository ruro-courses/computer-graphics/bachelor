#version 460
layout(row_major) uniform;

// IN
in VERT_FRAG { vec3 pos; } fin;
// OUT
layout(location = 0) out vec4 col;
// UNIFORM
uniform samplerCube U_texture;
// CODE
void main(void) { col = texture(U_texture, fin.pos); }
