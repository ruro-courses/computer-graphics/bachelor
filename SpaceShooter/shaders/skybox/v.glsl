#version 460
layout(row_major) uniform;

// IN
layout(location = 0) in vec3 vpos;

// OUT
out VERT_FRAG { vec3 pos; } vout;
out gl_PerVertex { vec4 gl_Position; };

// UNIFORM
uniform mat4 U_projMatrix;
uniform mat4 U_viewMatrix;

// CODE
void main(void)
{
    gl_Position = vec4(vpos, 1.0) * mat4(mat3(U_viewMatrix)) * U_projMatrix;
    vout.pos = vpos;
}
