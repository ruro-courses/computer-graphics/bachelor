#version 460
layout(row_major) uniform;

// IN
layout(location=0) in vec3 pos;
layout(location=1) in vec3 nor;
layout(location=2) in vec2 uvs;

// OUT
out VERT_FRAG {
    vec3 pos;
    vec3 nor;
    vec2 uvs;
} vout;
out gl_PerVertex { vec4 gl_Position; };

// UNIFORM
uniform mat4 U_projMatrix;
uniform mat4 U_viewMatrix;
uniform mat4 U_modlMatrix = mat4(1.0);

// CODE
void main(void)
{
    vec4 vert = vec4(pos, 1.0) * U_modlMatrix;

    vout.pos = vert.xyz/vert.w;
    vout.nor = nor * mat3(U_modlMatrix);
    vout.uvs = uvs;

    gl_Position = vert * U_viewMatrix * U_projMatrix;
}
