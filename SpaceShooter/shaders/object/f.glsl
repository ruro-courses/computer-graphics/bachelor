#version 460
layout(row_major) uniform;

// IN
in VERT_FRAG {
    vec3 pos;
    vec3 nor;
    vec2 uvs;
} fin;

// OUT
layout(location = 0) out vec4 col;

// UNIFORM
uniform sampler2D U_Tambient;
uniform sampler2D U_Tdiffuse;
uniform sampler2D U_Tspecular;
uniform mat4 U_viewMatrix;
uniform vec3 U_sunPos = vec3(0, 0, -1);
uniform vec3 U_Cambient = vec3(1, 1, 1);
uniform vec3 U_Cdiffuse = vec3(1, 1, 1);
uniform vec3 U_Cspecular = vec3(1, 1, 1);
uniform float U_shininess = 4.0;
uniform float U_dissolve = 1.0;

// SSBO
struct LightData
{
    vec4 pos;
    vec4 col;
};

layout(std430, binding = 0) buffer B_lightData { LightData L[]; };

// CODE
vec3 ViewPos(in vec3 Fpos)
{
    vec4 Opos = vec4(Fpos, 1) * U_viewMatrix;
    return -Opos.xyz/Opos.w;
}

vec3 LightPos(in vec3 Fpos, in int lightIndex)
{
    // Light -1 is the sun at inf, so it doesn't get translated
    if (lightIndex == -1) { return U_sunPos; }
    return L[lightIndex].pos.xyz - Fpos;
}

vec3 LightCol(in vec3 Lpos, in int lightIndex)
{
    // Light -1 is the sun at inf, so it's always the same brightness
    if (lightIndex == -1) { return vec3(1, 1, 1); }
    return (L[lightIndex].col.a / dot(Lpos, Lpos)) * L[lightIndex].col.rgb;
}

vec3 Illuminate(in vec3 Famb, in vec3 Fdif, in vec3 Fspe, in vec3 Fpos, in vec3 Ndir)
{
    vec3 total = Famb;

    for (int li = -1; li < L.length(); ++li)
    {
        const vec3 Lpos = LightPos(Fpos, li);
        const vec3 Ldir = normalize(Lpos);
        const float Idif = dot(Ndir, Ldir);
        if (Idif > 0)
        {
            // const vec3 Rdir = reflect(Ldir, Ndir);
            const vec3 Lcol = LightCol(Lpos, li);
            const vec3 Vdir = normalize(ViewPos(Fpos));
            const vec3 Hdir = normalize(Ldir + Vdir);
            float Ispe = dot(Ndir, Hdir);
            Ispe = (Ispe > 0) ? pow(Ispe, U_shininess) : 0;

            total.rgb += Lcol * (Idif * Fdif.rgb + Ispe * Fspe.rgb);
        }
    }

    return clamp(total, 0, 1);
}

void main(void)
{
    vec3 Famb = texture(U_Tambient, fin.uvs).rgb;
    vec3 Fdif = texture(U_Tdiffuse, fin.uvs).rgb;
    vec3 Fspe = texture(U_Tspecular, fin.uvs).rgb;
    Famb *= U_Cambient;
    Fdif *= U_Cdiffuse;
    Fspe *= U_Cspecular;

    vec3 ill = Illuminate(Famb, Fdif, Fspe, fin.pos, normalize(fin.nor));
    col = vec4(ill, U_dissolve);
}
