#version 460
layout(row_major) uniform;

// IN
in VERT_FRAG { vec2 tpos; vec2 spos; } fin;

// OUT
layout(location = 0) out vec4 col;

// UNIFORM
uniform int U_wrapColumns = 30;
uniform ivec2 U_offset = ivec2(4, 2);
uniform sampler2DArray U_fontTexture;
uniform sampler2D U_overlayTexture;

// SSBO
struct TextData { int ch; };
layout(std430, binding = 0) buffer B_textData { TextData S[]; };

// CODE
int chparse(in int ch)
{ return (32 > ch|| ch > 126) ? 0 : ch - 32; }

int posparse(inout vec2 tp)
{
    vec2 ch_pos;
    tp.x = modf(tp.x, ch_pos.x);
    tp.y = modf(tp.y, ch_pos.y);
    ch_pos -= U_offset;
    if (0 > ch_pos.x || ch_pos.x >= U_wrapColumns) return -1;
    return int(ch_pos.x + U_wrapColumns * ch_pos.y);
}

int chfetch(in int pos)
{ return 0 <= pos && pos < S.length() ? S[pos].ch : 0; }

void main(void)
{
    vec2 fr_pos = fin.tpos.xy;
    int pos = posparse(fr_pos);
    int ch = chparse(chfetch(pos));

    vec4 text_col = texture(U_fontTexture, vec3(fr_pos, ch));
    vec4 over_col = texture(U_overlayTexture, fin.spos);

    text_col.rgb = (1 - text_col.rgb);
    col.a = 1 - (1 - text_col.a)*(1 - over_col.a);
    col.rgb = text_col.a * text_col.rgb + (1-text_col.a)*over_col.rgb;
}
