#version 460
layout(row_major) uniform;

// IN
layout(location = 0) in vec3 vpos;

// OUT
out VERT_FRAG { vec2 tpos; vec2 spos; } vout;
out gl_PerVertex { vec4 gl_Position; };

// UNIFORM
uniform float U_fontAspect = 128f/209f;
uniform float U_overlayAspect = 9f/16f;
uniform int U_screenColumns = 128;

// CODE
void main(void)
{
    vec2 spos = vec2((1+vpos.x)/2, (1-vpos.y)/2);
    vout.tpos = U_screenColumns * vec2(1, U_overlayAspect*U_fontAspect) * spos;
    vout.spos = spos;
    gl_Position = vec4(vpos, 1);
}
