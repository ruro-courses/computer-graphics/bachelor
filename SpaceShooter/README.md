# 3D OpenGL Space Shooter

## Features:

|            Feature            | Notes / How to enable                                                                                                                                                                     | Points |
|:-----------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------:|
|  3 kinds of enemy spaceships  | Enemies will chase and shoot the player and receive damage from projectiles/collisions                                                                                                    |  ---   |
|     2 kinds of asteroids      | Basically, enemies, but they don't move or shoot the player                                                                                                                               |  ---   |
|         Player Weapon         | You can shoot by pressing LMB/RMB                                                                                                                                                         |  ---   |
|         Enemy Weapons         | Damage to player and enemies is applied based on collisions with objects/projectiles                                                                                                      |  ---   |
|        Realistic Space        | Movement through space is shown by rendering space dust particles                                                                                                                         |  ---   |
|           Reticule            | Your projectiles are released in the direction you are facing, a reticule is displayed for convenience                                                                                    |  ---   |
|      Status Info & Text       | Arbitrary text/UI overlays can be shown (Shows your current HP and points and game over screen)                                                                                           |  ---   |
|          Collisions           | Collisions are computed by bounding Sphere intersection between different objects (i.e. player & asteroid, enemy & projectile)                                                            |   +1   |
|      Arbitrary Movements      | The movements are not restricted to a plane (full 3D, see Keybindings)                                                                                                                    |   +1   |
|       Realistic Enemies       | The enemy ships and asteroids are composed of complex 3D polygon meshes                                                                                                                   |   +3   |
|       Textured Enemies        | The enemy ships and asteroids use textures                                                                                                                                                |   +1   |
|         Illumination          | Objects in the scene are dynamically illuminated by the star (modeled as a point light at infinity) and an arbitrary amount of local point lights, Blinn-Phong illumination model is used |  +2-5  |
|   Projectile Visualization    | Player and enemy weapon projectiles are visualized by 3d energy balls                                                                                                                     |   +2   |
| GPU Utilization for particles | Space particles are render using a Geometry shader on the GPU to avoid simulating them on the CPU                                                                                         |   +3   |

## Build and run:

#### Build
Requrements:
- `GCC` (tested on version `8.2.1`, must support `C++17`)
- `OpenGL` (version `4.6` or higher)
- `GLFW` (tested on version `3.2.1-2`)
  > Install via `apt-get install libglfw3-dev`
- `glm` (tested on version `0.9.9.5`)
  > Install via `apt-get install libglm-dev`
- `SOIL` (tested on version `1.16`)
  > Install via `apt-get install libsoil-dev`

####  !!! YOU NEED TO DOWNLOAD THE APPDATA FOLDER !!!

If you have downloaded the program from courses.graphics.cs.msu.ru, you
can find the link in the comments.

If you are using git, clone with LFS enabled.

Once you have all the assets in the appdata folder,
run the following commands to build the application.

``` bash
mkdir build
cd build
cmake ..
make
```

#### Run
``` bash
./main
```

## Keybindings:

|       Controls       | Actions                                                 |
|:--------------------:|:--------------------------------------------------------|
|        Mouse         | Rotate camera (free rotation)                           |
| CTRL+C/Backspace/ESC | Exit                                                    |
|     W/A/S/D/R/F      | Accelerate forward/left/backward/right/up/down          |
|       Y/G/H/J        | Rotate camera up/left/down/right (alternative to mouse) |
|         Q/E          | Rotate camera counter-clockwise/clockwise               |
|    LSHIFT/RSHIFT     | Increase movement speed (while held)                    |
|       LMB/RMB        | Shoot projectile                                        |

## Screenshots:

#### Space

![Space](screen01.jpg)

#### Asteroids

![Asteroids](screen02.jpg)

#### Spaceships

![Spaceships](screen03.jpg)
