#include "resources.hpp"

#include <iostream>


auto &ResourceStore::instance()
{
    static ResourceStore store;
    return store;
}

Shader &ResourceStore::getShader(const std::string &name)
{
    ResourceStore &I = instance();
    if (!I._shaders.count(name)) { I.loadShader(name); }
    return *I._shaders[name];
}

ObjectModel &ResourceStore::getModel(const std::string &name)
{
    ResourceStore &I = instance();
    if (!I._models.count(name)) { I.loadModel(name); }
    return *I._models[name];
}

Texture &ResourceStore::getTexture(const std::string &name)
{
    ResourceStore &I = instance();
    if (!I._textures.count(name)) { I.loadTexture(name); }
    return *I._textures[name];
}

std::unique_ptr<Image> loadImage(const std::string &name, std::size_t dot)
{
    // Guess DDS vs SOIL based on image extension
    if (dot == std::string::npos) { throw std::runtime_error("Texture " + name + " is missing extension."); }
    if (name.substr(dot) == ".dds") { return std::make_unique<DDSImage>(name); }
    else { return std::make_unique<SOILImage>(name); }
}

void ResourceStore::loadTexture(const std::string &name)
{
    std::size_t dot1 = name.find_last_of('.');
    std::size_t dot2 = name.find_last_of('.', dot1 - 1);
    // Guess 2D vs Cubemap vs Font based on second format
    bool is_not_2d = dot2 != std::string::npos;
    bool is_cubemap = is_not_2d && name.substr(dot2, dot1 - dot2) == ".cube";
    bool is_font = is_not_2d && name.substr(dot2, dot1 - dot2) == ".font";
    if (is_cubemap) { _textures.emplace(name, std::make_unique<Cubemap>(*loadImage(name, dot1))); }
    else if (is_font) { _textures.emplace(name, std::make_unique<Font>(*loadImage(name, dot1))); }
    else { _textures.emplace(name, std::make_unique<Texture2D>(*loadImage(name, dot1))); }
}

void ResourceStore::loadShader(const std::string &name) { _shaders.emplace(name, std::make_unique<Shader>(name)); }

void ResourceStore::loadModel(const std::string &name) { _models.emplace(name, std::make_unique<ObjectModel>(name)); }
