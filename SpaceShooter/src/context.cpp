#include <utility>

#include "context.hpp"

#include <iostream>
#include <sstream>


void glDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei, GLchar const *message, void const *)
{
    auto const src_str = [source]()
    {
        switch (source)
        {
            case GL_DEBUG_SOURCE_API: return "API";
            case GL_DEBUG_SOURCE_WINDOW_SYSTEM: return "WINDOW SYSTEM";
            case GL_DEBUG_SOURCE_SHADER_COMPILER: return "SHADER COMPILER";
            case GL_DEBUG_SOURCE_THIRD_PARTY: return "THIRD PARTY";
            case GL_DEBUG_SOURCE_APPLICATION: return "APPLICATION";
            case GL_DEBUG_SOURCE_OTHER: return "OTHER";
            default: return "UNKNOWN";
        }
    }();

    auto const type_str = [type]()
    {
        switch (type)
        {
            case GL_DEBUG_TYPE_ERROR: return "ERROR";
            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "DEPRECATED_BEHAVIOR";
            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: return "UNDEFINED_BEHAVIOR";
            case GL_DEBUG_TYPE_PORTABILITY: return "PORTABILITY";
            case GL_DEBUG_TYPE_PERFORMANCE: return "PERFORMANCE";
            case GL_DEBUG_TYPE_MARKER: return "MARKER";
            case GL_DEBUG_TYPE_OTHER: return "OTHER";
            default: return "UNKNOWN";
        }
    }();

    auto const severity_str = [severity]()
    {
        switch (severity)
        {
            case GL_DEBUG_SEVERITY_NOTIFICATION: return "NOTIFICATION";
            case GL_DEBUG_SEVERITY_LOW: return "LOW";
            case GL_DEBUG_SEVERITY_MEDIUM: return "MEDIUM";
            case GL_DEBUG_SEVERITY_HIGH: return "HIGH";
            default: return "UNKNOWN";
        }
    }();

    std::cout << '[' << src_str << ", " << type_str << ", " << severity_str << ", " << id << "]: " << message << '\n';

    if (type == GL_DEBUG_TYPE_ERROR) { abort(); }
}


std::string getVendorString()
{
    std::ostringstream vString;

    vString << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
    vString << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
    vString << "Version: " << glGetString(GL_VERSION) << std::endl;
    vString << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

    return vString.str();
}

Context &ctxtFromWindow(GLFWwindow *w) { return *static_cast<Context *>(glfwGetWindowUserPointer(w)); }

Controller &ctrlFromWindow(GLFWwindow *w) { return ctxtFromWindow(w).getController(); }

Context::Context(const char *title, hints_t extra_hints, int v_major, int v_minor)
{
    if (!glfwInit()) { throw std::runtime_error("Failed to initialize GLFW context"); }

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode *mode = glfwGetVideoMode(monitor);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, v_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, v_minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
#if defined(DEBUG)
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    for (const auto &h : extra_hints) { glfwWindowHint(h.first, h.second); }

    _window = glfwCreateWindow(mode->width, mode->height, title, monitor, nullptr);

    if (!_window)
    {
        throw std::runtime_error(
                "Failed to create GLFW window. Notice, that OpenGL version " +
                std::to_string(v_major) + "." + std::to_string(v_minor) +
                " or higher is required for this application."
        );
    }

    glfwMakeContextCurrent(_window);

    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) { throw std::runtime_error("Failed to initialize OpenGL context"); }

#if defined(DEBUG)
    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(glDebugCallback, nullptr);
    std::cout << getVendorString() << std::endl;
#endif

    glfwSwapInterval(1);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glfwGetWindowSize(_window, &_windowSize.x, &_windowSize.y);
    glfwSetCursorPos(_window, 0, 0);
    captureCursor(true);

    glfwSetWindowUserPointer(_window, this);
    glfwSetWindowSizeCallback(
            _window, [](GLFWwindow *w, int width, int height) { ctxtFromWindow(w)._windowSize = {width, height}; }
    );
}

Context::~Context() { glfwTerminate(); }

void Context::clear() const
{
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Context::swap() const { glfwSwapBuffers(_window); }

void Context::sync() const
{
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glFinish();
}

void Context::renderLoop(const condition_t &condition, const std::vector<procedure_t> &body) const
{
    const condition_t &_condition = condition ? condition : [] { return false; };
    while (!glfwWindowShouldClose(_window) && !_condition()) { render(body); }
}

void Context::render(const std::vector<procedure_t> &body) const
{
    clear();
    for (const auto &f : body) { f(); }
    sync();
    swap();
}

void Context::captureCursor(bool on) const { glfwSetInputMode(_window, GLFW_CURSOR, on ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL); }

void Context::close() const { glfwSetWindowShouldClose(_window, true); }

void Context::setController(Controller *controller)
{
    _controller = controller;
    glfwSetKeyCallback(
            _window,
            [](GLFWwindow *w, int k, int, int a, int m) { ctrlFromWindow(w).onKeyEvent(k, a, m); }
    );
    glfwSetCursorPosCallback(
            _window,
            [](GLFWwindow *w, double x, double y) { ctrlFromWindow(w).onMouseMove({y, x}); }
    );

    glfwSetMouseButtonCallback(
            _window, [](GLFWwindow *w, int b, int a, int) { ctrlFromWindow(w).onMouseEvent(b, a); }
    );
}

Controller &Context::getController() { return *_controller; }

glm::ivec2 Context::getWindowSize() const { return _windowSize; }
