#include "controller.hpp"


void Controller::onMouseMove(glm::vec2 pos)
{
    // Just record mouse movement, actual rotation is performed in tick
    glm::vec2 diff = (pos - mousePos);
    mouseRot += diff;
    mousePos = pos;
}

void Controller::onMouseEvent(int button, int action)
{
    if (action == GLFW_REPEAT) { return; }
    if ((button == GLFW_MOUSE_BUTTON_RIGHT || button == GLFW_MOUSE_BUTTON_LEFT)
        && action == GLFW_PRESS)
    {
        shoot = true;
    }
}

void Controller::onKeyEvent(int key, int action, int mods)
{
    // Record keybord input state changes
    float sign = 0;
    if (action == GLFW_PRESS) { sign = 1; }
    else if (action == GLFW_RELEASE) { sign = -1; }
    else { return; }

    switch (key)
    {
        // Movement
        case GLFW_KEY_A: sign *= -1;
            [[fallthrough]];
        case GLFW_KEY_D: keybdTra.x -= sign;
            break;

        case GLFW_KEY_F: sign *= -1;
            [[fallthrough]];
        case GLFW_KEY_R: keybdTra.y -= sign;
            break;

        case GLFW_KEY_W: sign *= -1;
            [[fallthrough]];
        case GLFW_KEY_S:keybdTra.z -= sign;
            break;

        case GLFW_KEY_H: sign *= -1;
            [[fallthrough]];
        case GLFW_KEY_Y:keybdRot.x -= sign;
            break;

        case GLFW_KEY_J: sign *= -1;
            [[fallthrough]];
        case GLFW_KEY_G:keybdRot.y -= sign;
            break;

        case GLFW_KEY_E: sign *= -1;
            [[fallthrough]];
        case GLFW_KEY_Q:keybdRot.z -= sign;
            break;

            // Increase/Decrease movement speed
        case GLFW_KEY_LEFT_SHIFT:[[fallthrough]];
        case GLFW_KEY_RIGHT_SHIFT: accelerate = action == GLFW_PRESS;
            break;

            // Close application on Escape, Ctrl+C or backspace
        case GLFW_KEY_C: if (!(mods & GLFW_MOD_CONTROL)) { break; }
            [[fallthrough]];
        case GLFW_KEY_ESCAPE:[[fallthrough]];
        case GLFW_KEY_BACKSPACE: exit = true;
            break;

        default: break;
    }
}

glm::vec3 Controller::getDeltaRotation()
{
    glm::vec3 _mouseRot = {mouseRot, 0};
    mouseRot = {0, 0};
    return keybdLookSensitivity * keybdRot + mouseLookSensitivity * _mouseRot;
}

glm::vec3 Controller::getDeltaTranslation() { return keybdMoveSensitivity * keybdTra; }

bool Controller::shouldExit() { return exit; }

bool Controller::isAccelerating() { return accelerate; }

bool Controller::shouldShoot()
{
    bool ret = shoot;
    shoot = false;
    return ret;
}

void Controller::update() { glfwPollEvents(); }
