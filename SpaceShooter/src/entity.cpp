#include <iostream>
#include "entity.hpp"
#include "world.hpp"

template<typename A, typename B>
auto L(bool isLocal, A a, B b)
{
    if (isLocal) { return a * b; }
    else { return b * a; }
}

void Entity::translateGlobal(glm::vec3 dX) { T += dX; }

void Entity::translateLocal(glm::vec3 dX) { T += L(isLocal, dX, R); }

void Entity::accelerateGlobal(glm::vec3 dV) { V += dV; }

void Entity::accelerateLocal(glm::vec3 dV) { V += L(isLocal, dV, R); }

void Entity::rotateGlobal(glm::vec3 axis, float dA) { R = glm::rotate(R, dA, axis); }

void Entity::rotateLocal(glm::quat dR) { R = L(isLocal, dR, R); }

void Entity::rotateGlobal(glm::quat dR) { R = L(isLocal, R, dR); }

glm::mat4 Entity::getModelMatrix() const { return L(isLocal, glm::mat4_cast(R), tra_from_vector(isLocal ? T : -T)); }

void Entity::tick(bool enforceSoftLimit)
{
    float lV = glm::length(V);
    if (enforceSoftLimit && lV > softSpeedLimit) { V *= (softSpeedLimit / lV); }
    else if (lV > hardSpeedLimit) { V *= (hardSpeedLimit / lV); }
    T += V;
}

bool Entity::isDead() const { return health <= 0; }

void collide(Entity &e1, Entity &e2, float e1_dmulti, float e2_dmulti)
{
    if (&e1 != &e2 && glm::length(e1.T - e2.T) < (e1.radius + e2.radius))
    {
        e1.health -= e1_dmulti;
        e2.health -= e2_dmulti;
    }
}

void Entity::rotateSLERP(glm::quat target, float d) { R = glm::slerp(R, target, d); }

void Entity::shoot(World &w)
{
    glm::vec3 tr = T + (isLocal ? 2.0f : -1.0f) * L(isLocal, glm::vec3({0, 0, radius + 2}), R);
    glm::vec3 ac = V + (isLocal ? 2.0f : -1.0f) * L(isLocal, glm::vec3({0, 0, 1}), R);
    w.spawn("projectile", tr, ac, R);
}

