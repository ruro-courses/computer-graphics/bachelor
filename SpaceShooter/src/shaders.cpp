#include "shaders.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/stat.h>


const char *statusTypeName(GLenum type)
{
    switch (type)
    {
        case GL_COMPILE_STATUS: return "compilation";
        case GL_LINK_STATUS: return "linking";
        case GL_VALIDATE_STATUS: return "validation";
        default: return "unknown stage";
    }
}


void checkStatus(GLuint program, GLenum statusType, const char *shadername, const char *filename = nullptr)
{
#if defined(DEBUG)
    GLint status, logLength;
    if (filename)
    {
        glGetShaderiv(program, statusType, &status);
        glGetShaderiv(program, GL_INFO_LOG_LENGTH, &logLength);
    }
    else
    {
        glGetProgramiv(program, statusType, &status);
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
    }

    GLchar logMessage[logLength];
    if (filename) { glGetShaderInfoLog(program, logLength, nullptr, logMessage); }
    else { glGetProgramInfoLog(program, logLength, nullptr, logMessage); }

    if (status != GL_TRUE)
    {
        std::ostringstream ss;
        ss << "Shader program " << statusTypeName(statusType) << " failed." << std::endl;
        if (shadername) { ss << "In shader " << shadername << std::endl; }
        if (filename) { ss << "In file " << filename << std::endl; }
        if (logLength) { ss << logMessage << std::endl; }
        throw std::runtime_error(ss.str());
    }
    else if (logLength) { std::cerr << logMessage << std::endl; }
#endif
}


GLint getUniformLocation(GLuint shaderProgram, const std::string &location)
{
    GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
    if (uniformLocation == -1) { throw std::runtime_error("Uniform  " + location + " not found"); }
    return uniformLocation;
}

GLuint loadShaderStage(GLenum type, const std::string &filename)
{
    std::ifstream fs(filename);
    if (!fs.is_open()) { throw std::runtime_error("ERROR: Could not read shader from " + filename); }
    std::string shaderText{std::istreambuf_iterator<char>(fs), {}};
    const char *shaderSrc = shaderText.c_str();

    GLuint shaderStage = glCreateShader(type);
    glShaderSource(shaderStage, 1, &shaderSrc, nullptr);
    glCompileShader(shaderStage);

    return shaderStage;
}

const std::unordered_map<GLenum, std::string> _shaderDefaults = {
        {GL_VERTEX_SHADER,          "/v.glsl"},
        {GL_FRAGMENT_SHADER,        "/f.glsl"},
        {GL_GEOMETRY_SHADER,        "/g.glsl"},
        {GL_TESS_CONTROL_SHADER,    "/tc.glsl"},
        {GL_TESS_EVALUATION_SHADER, "/te.glsl"},
        {GL_COMPUTE_SHADER,         "/c.glsl"},
};

Shader::Shader(const std::string &name) : shaderPath("shaders/" + name), shaderProgram(), shaderSources(), shaderStages()
{
    struct stat buffer = {};
    for (const auto &d : _shaderDefaults)
    {
        // If file exists in shader folder
        std::string df = shaderPath + d.second;
        if (!stat(df.c_str(), &buffer)) { shaderSources[d.first] = df; }
    }
    initialize();
}

Shader::~Shader()
{
    disable();
    for (const auto &src : shaderStages)
    {
        glDetachShader(shaderProgram, src.second);
        glDeleteShader(src.second);
    }
    glDeleteProgram(shaderProgram);
}

void Shader::initialize()
{
    shaderProgram = glCreateProgram();
    for (const auto &src : shaderSources)
    {
        GLuint shaderStage = loadShaderStage(src.first, src.second);
        checkStatus(shaderStage, GL_COMPILE_STATUS, shaderPath.c_str(), src.second.c_str());
        glAttachShader(shaderProgram, shaderStage);
        shaderStages[src.first] = shaderStage;
    }
    glLinkProgram(shaderProgram);
    checkStatus(shaderProgram, GL_LINK_STATUS, shaderPath.c_str());
#if defined(DEBUG)
    glValidateProgram(shaderProgram);
    checkStatus(shaderProgram, GL_VALIDATE_STATUS, shaderPath.c_str());
#endif
}

void Shader::enable() const { glUseProgram(shaderProgram); }

void Shader::disable() const { glUseProgram(0); }

#define _SetUniformImpl(T, F, ...)                                          \
void Shader::setUniform(const std::string &location, T) const  \
{ glUniform##F(getUniformLocation(shaderProgram, location), __VA_ARGS__); }

_SetUniformImpl(int value, 1i, value)

_SetUniformImpl(unsigned int value, 1ui, value)

_SetUniformImpl(float value, 1f, value)

_SetUniformImpl(double value, 1d, value)

_SetUniformImpl(glm::mat4x4 value, Matrix4fv, 1, GL_TRUE, glm::value_ptr(value));

_SetUniformImpl(glm::vec3 value, 3fv, 1, glm::value_ptr(value))

_SetUniformImpl(const float value[3], 3fv, 1, value)

_SetUniformImpl(glm::ivec2 value, 2iv, 1, glm::value_ptr(value))
