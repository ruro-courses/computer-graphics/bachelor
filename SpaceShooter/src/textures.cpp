#include "textures.hpp"

#include <SOIL/SOIL.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>


SOILImage::SOILImage(std::string name)
{
    data = SOIL_load_image(("appdata/textures/" + name).c_str(), &w, &h, nullptr, SOIL_LOAD_RGBA);
    if (!data) { throw std::runtime_error("Couldn't load texture " + name + ".\n"); }
    compressed = false;
    size = w * h;
}

SOILImage::~SOILImage()
{
    if (data)
    {
        SOIL_free_image_data(data);
        data = nullptr;
    }
}

#define DDS_HEADER_SIZE  128
#define DX10_HEADER_SIZE 20
#define DDS_SIGNATURE    0x20534444 // "DDS "
#define FORMAT_CODE_RAW8 0          // "RGBA8"
#define FORMAT_CODE_DX10 0x30315844 // "DX10"
#define FORMAT_CODE_DXT1 0x31545844 // "DXT1"
#define FORMAT_CODE_DXT3 0x33545844 // "DXT3"
#define FORMAT_CODE_DXT5 0x35545844 // "DXT5"

DDSImage::DDSImage(std::string name)
{
    struct stat st = {};
    std::string filename = "appdata/textures/" + name;
    stat(filename.c_str(), &st);
    fsize = static_cast<size_t>(st.st_size);

    fd = open(filename.c_str(), O_RDONLY, 0);
    if (fd == -1) { throw std::runtime_error("Couldn't open texture " + name + ".\n"); }

    fmap = mmap(nullptr, fsize, PROT_READ, MAP_PRIVATE | MAP_POPULATE, fd, 0);
    if (fmap == MAP_FAILED) { throw std::runtime_error("Couldn't mmap texture " + name + ".\n"); }

    auto *header = static_cast<std::uint32_t *>(fmap);
    if (fsize < DDS_HEADER_SIZE || header[0] != DDS_SIGNATURE) { throw std::runtime_error("Corrupted DDS format for texture " + name + ".\n"); }

    std::size_t header_size = DDS_HEADER_SIZE;
    switch (header[21])
    {
        case FORMAT_CODE_DX10: header_size += DX10_HEADER_SIZE; break;
        case FORMAT_CODE_RAW8: break;
        case FORMAT_CODE_DXT1:
        {
            storage_format = loading_format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
            compressed = true;
            break;
        }
        case FORMAT_CODE_DXT3:
        {
            storage_format = loading_format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
            compressed = true;
            break;
        }
        case FORMAT_CODE_DXT5:
        {
            storage_format = loading_format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
            compressed = true;
            break;
        }
        default: throw std::runtime_error("DDS formats other than DXT1,3,5 and RGBA8 are not supported for texture " + name + ".\n");
    }

    data = static_cast<GLubyte *>(fmap) + header_size;
    w = header[4];
    h = header[3];
    size = w * h;
}

DDSImage::~DDSImage()
{
    if (fmap)
    {
        munmap(fmap, fsize);
        fmap = data = nullptr;
    }
    if (fd != -1)
    {
        close(fd);
        fd = -1;
    }
}

Texture::Texture(GLenum target, const Image &img, TextureParameters params, GLint _levels) : levels(_levels)
{
    if (!levels)
    {
        levels = 1;
        GLint sides = img.w | img.h;
        while (sides >> levels) { levels++; }
    }

    glCreateTextures(target, 1, &name);
    for (auto kv : params) { glTextureParameteri(name, kv.first, kv.second); }
}

Texture::~Texture() { glDeleteTextures(1, &name); }

void Texture::bind(GLuint unit) { glBindTextureUnit(unit, name); }

Texture2D::Texture2D(const Image &img, TextureParameters params, GLint _levels) :
        Texture(GL_TEXTURE_2D, img, std::move(params), _levels)
{
    glTextureStorage2D(name, levels, img.storage_format, img.w, img.h);

    if (img.compressed) { glCompressedTextureSubImage2D(name, 0, 0, 0, img.w, img.h, img.loading_format, img.size, img.data); }
    else { glTextureSubImage2D(name, 0, 0, 0, img.w, img.h, img.loading_format, GL_UNSIGNED_BYTE, img.data); }

    glGenerateTextureMipmap(name);
}

Cubemap::Cubemap(const Image &img, TextureParameters params, GLint _levels) :
        Texture(GL_TEXTURE_CUBE_MAP, img, std::move(params), _levels)
{
    glTextureStorage2D(name, levels, img.storage_format, img.w, img.h);

    if (img.compressed) { glCompressedTextureSubImage3D(name, 0, 0, 0, 0, img.w, img.h, 6, img.loading_format, 6 * img.size, img.data); }
    else { glTextureSubImage3D(name, 0, 0, 0, 0, img.w, img.h, 6, img.loading_format, GL_UNSIGNED_BYTE, img.data); }

    glGenerateTextureMipmap(name);
}

Font::Font(const Image &img, GLsizei chrs, TextureParameters params, GLint _levels) :
        Texture(GL_TEXTURE_2D_ARRAY, img, std::move(params), 1)
{
    (void)_levels;
    glTextureStorage3D(name, 1, img.storage_format, img.w, img.h, chrs);

    if (img.compressed) { glCompressedTextureSubImage3D(name, 0, 0, 0, 0, img.w, img.h, chrs, img.loading_format, chrs * img.size, img.data); }
    else { glTextureSubImage3D(name, 0, 0, 0, 0, img.w, img.h, chrs, img.loading_format, GL_UNSIGNED_BYTE, img.data); }

    //glGenerateTextureMipmap(name);
}
