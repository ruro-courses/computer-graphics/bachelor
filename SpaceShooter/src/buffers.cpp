#include "buffers.hpp"


VAO::~VAO()
{
    glDeleteVertexArrays(1, &names.VAO);
    glDeleteBuffers(1, &names.VBO);
}

void VAO::draw(GLenum primitive, GLsizei instance_count)
{
    glBindVertexArray(names.VAO);
    glDrawArraysInstanced(primitive, 0, vert_count, instance_count);
    glBindVertexArray(0);
}

VAO &VAO::operator=(VAO &&that) noexcept
{
    std::swap(this->names, that.names);
    std::swap(this->vert_count, that.vert_count);
    return *this;
}

VAO::VAO(VAO &&that) noexcept : names(that.names), vert_count(that.vert_count)
{
    that.names = {};
    that.vert_count = 0;
}
