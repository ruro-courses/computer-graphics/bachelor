#include "interface.hpp"


PlainVert PlainVert::conversion(const tinyobj::attrib_t &a, const tinyobj::index_t &i)
{
    return {.pos = glm::make_vec3(&a.vertices[3 * i.vertex_index])};
}

ObjectVert ObjectVert::conversion(const tinyobj::attrib_t &a, const tinyobj::index_t &i)
{
    glm::vec2 uvs = safe_conversion2(a.texcoords, static_cast<size_t>(i.texcoord_index));
    uvs.y = 1-uvs.y;
    return {
            .pos = safe_conversion3(a.vertices, static_cast<size_t>(i.vertex_index)),
            .nor = safe_conversion3(a.normals, static_cast<size_t>(i.normal_index)),
            .uvs = uvs
    };
}

