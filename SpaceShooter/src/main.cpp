#include "context.hpp"
#include "resources.hpp"

#include <iostream>
#include <sstream>


int main(int argc, char **argv)
{
    int MSAA = 0;
    if (argc == 2)
    {
        MSAA = std::stoi(argv[1], nullptr, 10);
        std::cout << "Using " << MSAA << "xMSAA" << std::endl;
    }
    else if (argc != 1)
    {
        std::cout << "Usage:" << std::endl;
        std::cout << "\t" << argv[0] << " [MSAA level]" << std::endl;
    }

    Context ctxt = Context("OpenGL Space Shooter", {{GLFW_SAMPLES, MSAA}});
    Controller ctrl = Controller();
    UIModel ui = UIModel();
    World world = World(ctrl, ctxt, ui);

    ctxt.setController(&ctrl);

    int skybox_id = 1;
    std::vector<glm::vec3> skybox_suns = {
            {0.99769553,  -0.06160214, 0.02843936},
            {-0.94142901, 0.20732841,  0.26594426}
    };
    world.setSkybox("skybox/stars" + std::to_string(skybox_id) + ".cube.dds", skybox_suns[skybox_id - 1], 512);

    world.enable("asteroid.1");
    world.enable("asteroid.2");
    world.enable("spaceship.1");
    world.enable("spaceship.2");
    world.enable("spaceship.3");

    ctxt.renderLoop(
            [&]() { return ctrl.shouldExit() || world.playerDead(); },
            std::vector<procedure_t>(
                    {
                            [&]() { world.draw(); },
                            [&]() { ctrl.update(); },
                            [&]() { world.tick(); },
                    }
            )
    );
    if (world.playerDead())
    {
        UIModel gameover = UIModel("gameover.dds");
        ctxt.renderLoop(
                [&]() { return ctrl.shouldExit(); },
                std::vector<procedure_t>(
                        {
                                [&]()
                                {
                                    gameover.shader.enable();
                                    gameover.draw();
                                    gameover.shader.disable();
                                },
                                [&]() { ctrl.update(); },
                        }
                )
        );
    }
}
