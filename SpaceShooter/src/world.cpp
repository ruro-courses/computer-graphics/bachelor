#include "world.hpp"

#include <sstream>
#include <iostream>
#include <iomanip>


World::World(Controller &_ctrl, Context &_ctxt, UIModel &_ui) : ctrl(_ctrl), ctxt(_ctxt), ui(_ui)
{ player.isLocal = true; player.radius = 10; }

void World::spawn(const std::string &name, glm::vec3 translation, glm::vec3 acceleration, glm::quat rotation)
{
    Entity &e = objects[name].emplace_back();
    e.radius = ResourceStore::getModel(name).getRadius();
    e.translateGlobal(translation);
    e.rotateGlobal(rotation);
    e.accelerateGlobal(acceleration);
    float lA = glm::length(acceleration);
    e.softSpeedLimit = (lA > e.softSpeedLimit) ? lA : e.softSpeedLimit;
    e.hardSpeedLimit = (lA > e.hardSpeedLimit) ? lA : e.hardSpeedLimit;
}

void World::draw()
{
    static glm::mat4 lastView = player.getModelMatrix();
    glm::mat4 viewMatrix = player.getModelMatrix();

    glm::ivec2 ws = ctxt.getWindowSize();
    const float fov = 3.141592654f / 4.0f;
    const float zNear = 0.01f;
    const float zFar = 10000.0f;
    glm::mat4 projMatrix = glm::perspectiveFov(fov, static_cast<float>(ws.x), static_cast<float>(ws.y), zNear, zFar);

    if (skybox)
    {
        skybox->shader.enable();
        skybox->shader.setUniform("U_projMatrix", projMatrix);
        skybox->shader.setUniform("U_viewMatrix", viewMatrix);
        skybox->draw();
        skybox->shader.disable();
    }

    if (skydust)
    {
        skydust->shader.enable();
        skydust->shader.setUniform("U_projMatrix", projMatrix);
        skydust->shader.setUniform("U_viewMatrix", viewMatrix);
        skydust->shader.setUniform("U_traceMatrix", viewMatrix * glm::inverse(lastView));
        skydust->draw();
        skydust->shader.disable();
    }

    Shader &object_shader = ResourceStore::getShader("object");
    object_shader.enable();
    object_shader.setUniform("U_projMatrix", projMatrix);
    object_shader.setUniform("U_viewMatrix", viewMatrix);
    if (skybox) { object_shader.setUniform("U_sunPos", skybox->sun); }
    if (lights) { lights->bind(0); }
    for (auto &objGroup : objects) { ResourceStore::getModel(objGroup.first).draw(objGroup.second); }
    object_shader.disable();

    ui.shader.enable();
    std::ostringstream health;
    health
    << "      Health: "
    << std::setfill('0') << std::setw(3)
    << static_cast<int>(std::round(player.health * 100))
    << "/100 hp";

    std::ostringstream points;
    points
    << "      Points: "
    << std::setfill('0') << std::setw(7)
    << score
    << "pts";

    ui.setText({"", "", "        Current Status", "", health.str(), "", points.str()});
    ui.draw();
    ui.shader.disable();

    lastView = viewMatrix;
}

void World::tick()
{
    static sim_time_t last = sim_clock_t::now();
    while (sim_clock_t::now() - last >= sim_tick)
    {
        tickMonotonic();
        last += sim_tick;
    }
}

void World::tickMonotonic()
{
    bool accel = ctrl.isAccelerating();
    player.rotateLocal(ctrl.getDeltaRotation());
    player.accelerateLocal((1.0f / simTPS) * (accel ? player.hardSpeedLimit : 1.0f) * ctrl.getDeltaTranslation());
    player.tick(!accel);
    if (ctrl.shouldShoot()) { player.shoot(*this); }
    for (auto &obj_group : objects)
    {
        auto &grp = obj_group.second;
        if (obj_group.first != "projectile")
        {
            if (obj_group.first.rfind("asteroid") != 0)
            {
                populate(obj_group.first, 4);
                for (auto &obj : grp)
                {
                    obj.rotateSLERP(glm::quatLookAt(glm::normalize(player.T - obj.T), glm::vec3({0, 1, 0})), 1.0f / simTPS);
                    obj.accelerateLocal(glm::vec3({0, 0, -1.0f/simTPS}));
                    if (glm::linearRand(0.0f, 1.0f) < 1.0f/simTPS) { obj.shoot(*this); }
                }
            }
            else { populate(obj_group.first, 20); }
        }
        for (auto &obj : grp) { obj.tick(); }
    }
    for (auto &obj_group1 : objects)
    {
        auto &grp1 = obj_group1.second;
        for (auto &obj1 : grp1)
        {
            for (auto &obj_group2 : objects)
            {
                auto &grp2 = obj_group2.second;
                for (auto &obj2 : grp2) { collide(obj1, obj2); }
            }
            collide(player, obj1, 0.1f);
        }
        grp1.erase(
                std::remove_if(
                        grp1.begin(), grp1.end(), [&](Entity &ent)
                        {
                            bool tooFar = glm::length(player.T - ent.T) > worldSize;
                            bool dead = ent.isDead();
                            score += dead * (obj_group1.first != "projectile");
                            return tooFar || dead;
                        }
                ), grp1.end()
        );
    }
}

void World::setSkybox(const std::string &name, glm::vec3 sunPosition, std::size_t numParticles)
{
    skybox = std::make_unique<SkyboxModel>(name, sunPosition);
    skydust = std::make_unique<SkydustModel>(numParticles);
}

bool World::playerDead() { return player.isDead(); }

void World::enable(const std::string &name) { objects[name]; }

void World::populate(const std::string &name, std::size_t count)
{
    std::size_t num = objects[name].size();
    if (num >= count) { return; }
    num = count - num;
    for (std::size_t i = 0; i < num; ++i)
    {
        glm::vec3 pos = player.T + glm::ballRand(worldSize);
        glm::vec3 dir = glm::sphericalRand(1.0f);
        glm::quat rot = glm::quatLookAt(dir, glm::vec3({0, 1, 0}));
        spawn(name, pos, {0, 0, 0}, rot);
    }
}
