#include "models.hpp"


#define TINYOBJLOADER_IMPLEMENTATION


#include <OBJ/tinyobj.hpp>
#include <iostream>


template<typename T>
float loadModel(const std::string &name, std::vector<tinyobj::material_t> &mats, std::vector<VAO> &vaos)
{
    std::string error;
    std::string mod_path = "appdata/models/" + name + '/';
    std::string tex_path = "appdata/textures/" + name + '/';
    std::string obj_path = mod_path + "o.obj";

    std::vector<tinyobj::shape_t> shapes;
    tinyobj::attrib_t attrs;

    bool success = tinyobj::LoadObj(&attrs, &shapes, &mats, &error, obj_path.c_str(), mod_path.c_str());
    if (!success || !error.empty()) { throw std::runtime_error("Couldn't load model " + name + ":\n" + error); }

    std::size_t mat_count = mats.size();

    std::vector<std::vector<T>> _vaos;
    _vaos.resize(mat_count > 0 ? mat_count : 1);

    for (auto &sh : shapes)
    {
        auto &nfv = sh.mesh.num_face_vertices;

#if defined(DEBUG)
        for (auto nf : nfv) { if (nf != 3) { throw std::runtime_error("Failed triangulation of model " + name + ".\n"); } }
#endif

        std::size_t num_faces = nfv.size();
        for (std::size_t f_id = 0; f_id < num_faces; ++f_id)
        {
            int m_id = sh.mesh.material_ids[f_id];
            auto &verts = _vaos[m_id >= 0 ? m_id : 0];
            for (std::size_t v_id = 0; v_id < 3; ++v_id)
            {
                auto &idxs = sh.mesh.indices[3 * f_id + v_id];
                verts.emplace_back(T::conversion(attrs, idxs));
            }
        }
    }

    float radius = 0;
    for (auto &vao : _vaos)
    {
        for (auto &v : vao)
        { radius = std::max(radius, glm::length(v.pos)); }
        vaos.emplace_back(vao);
    }
    for (auto &mat : mats)
    {
        if (mat.ambient_texname.empty()) { mat.ambient_texname = "single.color.dds"; }
        if (mat.diffuse_texname.empty()) { mat.diffuse_texname = "single.color.dds"; }
        if (mat.specular_texname.empty()) { mat.specular_texname = "single.color.dds"; }
    }
    return radius;
}

ObjectModel::ObjectModel(const std::string &name) { radius = loadModel<ObjectVert>(name, mats, vaos); }

void ObjectModel::draw()
{
    Shader &shader = ResourceStore::getShader("object");
    drawStart(shader);
    std::size_t vsize = vaos.size();
    for (std::size_t vidx = 0; vidx < vsize; ++vidx)
    {
        bindVAO(shader, vidx);
        vaos[vidx].draw();
    }
}

void ObjectModel::draw(const std::vector<Entity> &MS)
{
    Shader &shader = ResourceStore::getShader("object");
    drawStart(shader);
    std::size_t vsize = vaos.size();
    for (std::size_t vidx = 0; vidx < vsize; ++vidx)
    {
        bindVAO(shader, vidx);
        for (auto &M : MS)
        {
            shader.setUniform("U_modlMatrix", M.getModelMatrix());
            vaos[vidx].draw();
        }
    }
}

void ObjectModel::drawStart(Shader &shader)
{
    shader.setUniform("U_Tambient", 0);
    shader.setUniform("U_Tdiffuse", 1);
    shader.setUniform("U_Tspecular", 2);
}

void ObjectModel::bindVAO(Shader &shader, size_t vidx)
{
    tinyobj::material_t &mat = mats[vidx];
    ResourceStore::getTexture(mat.ambient_texname).bind(0);
    ResourceStore::getTexture(mat.diffuse_texname).bind(1);
    ResourceStore::getTexture(mat.specular_texname).bind(2);
    shader.setUniform("U_Cambient", mat.ambient);
    shader.setUniform("U_Cdiffuse", mat.diffuse);
    shader.setUniform("U_Cspecular", mat.specular);
    shader.setUniform("U_dissolve", mat.dissolve);
    shader.setUniform("U_shininess", mat.shininess);
}

float ObjectModel::getRadius() { return radius; }

SkyboxModel::SkyboxModel(const std::string &name, glm::vec3 sunPosition) :
        shader(ResourceStore::getShader("skybox")), sun(sunPosition), texture(ResourceStore::getTexture(name))
{
    std::vector<tinyobj::material_t> mats;
    std::vector<VAO> vaos;
    loadModel<PlainVert>("skybox", mats, vaos);
    if (vaos.size() != 1) { throw std::runtime_error("Unable to load the skybox box model."); }
    std::swap(vao, vaos[0]);
}

void SkyboxModel::draw()
{
    texture.bind(0);
    glDepthMask(GL_FALSE);
    vao.draw();
    glDepthMask(GL_TRUE);
}

SkydustModel::SkydustModel(std::size_t num_particles) : shader(ResourceStore::getShader("skydust"))
{
    std::vector<PlainVert> pv;
    pv.resize(num_particles);
    for (auto &p : pv) { p.pos = glm::linearRand(glm::vec3{-1, -1, -1}, glm::vec3{1, 1, 1}); }
    vao = VAO(pv);
}

void SkydustModel::draw() { vao.draw(GL_POINTS); };

UIModel::UIModel(const std::string &_overlay, glm::ivec2 _size) :
        shader(ResourceStore::getShader("ui")),
        text(SSBO<TextData>(static_cast<std::size_t>(_size.x * _size.y))),
        size(_size),
        font(ResourceStore::getTexture("space.font.dds")),
        overlay(ResourceStore::getTexture(_overlay))
{
    std::vector<tinyobj::material_t> mats;
    std::vector<VAO> vaos;
    loadModel<PlainVert>("screen", mats, vaos);
    if (vaos.size() != 1) { throw std::runtime_error("Unable to load the screen model."); }
    std::swap(screen, vaos[0]);
}

void UIModel::draw()
{
    shader.setUniform("U_fontTexture", 0);
    shader.setUniform("U_overlayTexture", 1);
    text.bind(0);
    font.bind(0);
    overlay.bind(1);
    screen.draw();
}

void UIModel::setText(const std::vector<std::string> &_text)
{
    std::size_t lines = _text.size();
    for (std::size_t line = 0; line < static_cast<std::size_t>(size.y); ++line)
    {
        std::size_t chars =
                line < lines
                ? _text[line].length()
                : 0;
        for (std::size_t ch = 0; ch < static_cast<std::size_t>(size.x); ++ch)
        {
            text.data[line * size.x + ch].ch =
                    ch < chars
                    ? _text[line][ch]
                    : 0;
        }
    }
}
