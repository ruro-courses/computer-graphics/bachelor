#pragma once

class World;


#include "math.hpp"
#include "controller.hpp"
#include "resources.hpp"
#include "interface.hpp"
#include "buffers.hpp"
#include "entity.hpp"

#include <memory>
#include <chrono>


// Simulation ticks per second.
constexpr int simTPS = 120;
using sim_clock_t = std::chrono::high_resolution_clock;
using sim_time_t = sim_clock_t::time_point;
using sim_duration_t = sim_clock_t::duration;
using sim_tick_t = std::chrono::duration<float, std::ratio<1, simTPS>>;
constexpr sim_duration_t sim_tick = std::chrono::duration_cast<sim_duration_t>(sim_tick_t{1});

constexpr float worldSize = 1000.0f;

class World
{
public:
    explicit World(Controller &_ctrl, Context &_ctxt, UIModel &_ui);
    void tick();
    void draw();

    void spawn(
            const std::string &name,
            glm::vec3 translation = {0, 0, 0},
            glm::vec3 acceleration = {0, 0, 0},
            glm::quat rotation = glm::quat({0, 0, 0})
    );
    void populate(const std::string &name, std::size_t count);
    void enable(const std::string &name);
    void setSkybox(const std::string &name, glm::vec3 sunPosition, std::size_t numParticles);
    bool playerDead();

private:
    void tickMonotonic();;

    Controller &ctrl;
    Context &ctxt;
    UIModel &ui;
    std::unique_ptr<SSBO<LightData>> lights;
    std::unique_ptr<SkyboxModel> skybox;
    std::unique_ptr<SkydustModel> skydust;
    std::unordered_map<std::string, std::vector<Entity>> objects;
    Entity player;
    int score = 0;
};
