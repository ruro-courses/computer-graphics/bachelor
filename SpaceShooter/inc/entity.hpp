#pragma once

struct Entity;
class World;


#include "math.hpp"


void collide(Entity &e1, Entity &e2, float e1_dmulti=1, float e2_dmulti=1);


struct Entity
{
    explicit Entity() = default;

    void accelerateGlobal(glm::vec3 dV);
    void accelerateLocal(glm::vec3 dV);
    void translateGlobal(glm::vec3 dX);
    void translateLocal(glm::vec3 dX);
    void rotateLocal(glm::quat dR);
    void rotateSLERP(glm::quat target, float d);
    void rotateGlobal(glm::vec3 axis, float dA);
    void rotateGlobal(glm::quat dR);

    void tick(bool enforceSoftLimit = true);
    void shoot(World &w);
    glm::mat4 getModelMatrix() const;
    bool isDead() const;

    // Translation, rotation and other properties of the entity
    float radius = 1.0f;
    float health = 1.0f;
    float softSpeedLimit = 1.0f;
    float hardSpeedLimit = 4.0f;
    bool isLocal = false; // Player locality should be backwards.
    glm::vec3 V = glm::vec3(0);
    glm::vec3 T = glm::vec3(0);
    glm::quat R = glm::quat({0, 0, 0});

    friend void collide(Entity &e1, Entity &e2, float e1_dmulti, float e2_dmulti);
};
