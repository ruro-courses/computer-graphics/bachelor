#pragma once


class VAO;


template<typename T> class SSBO;


#include "context.hpp"
#include "interface.hpp"

#include <vector>


const GLbitfield _defaultStorageMode = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT |
                                       GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT |
                                       GL_DYNAMIC_STORAGE_BIT | GL_CLIENT_STORAGE_BIT;
const GLenum _defaultMapMode = GL_READ_WRITE;


class VAO
{
    struct { GLuint VAO; GLuint VBO; } names;
    GLsizei vert_count;

public:
    template<typename T>
    explicit VAO(const std::vector<T> &vert_init) : names()
    {
        vert_count = static_cast<GLsizei>(vert_init.size());
        glCreateVertexArrays(1, &names.VAO);
        glCreateBuffers(1, &names.VBO);

        glNamedBufferData(names.VBO, vert_count * sizeof(T), vert_init.data(), GL_STATIC_DRAW);
        glVertexArrayVertexBuffer(names.VAO, 0, names.VBO, 0, sizeof(T));

        for (GLuint i = 0; i < std::size(T::layout); i++)
        {
            field_t F = T::layout[i];
            glEnableVertexArrayAttrib(names.VAO, i);
            glVertexArrayAttribFormat(names.VAO, i, F.count, F.type, GL_FALSE, F.offset);
            glVertexArrayAttribBinding(names.VAO, i, 0);
        }
    }

    explicit VAO() = default;
    ~VAO();
    void draw(GLenum primitive = GL_TRIANGLES, GLsizei instance_count = 1);
    VAO(const VAO &) = delete;
    VAO &operator=(const VAO &) = delete;
    VAO(VAO &&) noexcept;
    VAO &operator=(VAO &&) noexcept;
};


template<typename T>
class SSBO
{
    GLuint name{};

public:
    GLsizei size;
    T *data{};

    explicit SSBO(const std::size_t _size, GLbitfield storageMode = _defaultStorageMode, GLenum mapMode = _defaultMapMode)
    {
        size = static_cast<GLsizei>(_size);
        glCreateBuffers(1, &name);
        glNamedBufferStorage(name, size * sizeof(T), nullptr, storageMode);
        data = static_cast<T *>(glMapNamedBuffer(name, mapMode));
    }

    ~SSBO() { glDeleteBuffers(1, &name); }

    void bind(GLuint binding) { glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, name); }

    SSBO(const SSBO &) = delete;
    SSBO &operator=(const SSBO &) = delete;
};
