#pragma once

#ifndef GLFW_DLL
    #define GLFW_DLL
#endif


#include <GLAD/glad.h>
#include <GLFW/glfw3.h>
#include <unordered_map>
#include <functional>

class Context;

using hints_t = std::unordered_map<int, int>;
using procedure_t = std::function<void(void)>;
using condition_t = std::function<bool(void)>;


#include "math.hpp"
#include "controller.hpp"


class Context
{
public:
    explicit Context(const char *title, hints_t extra_hints = {}, int v_major = 4, int v_minor = 6);

    ~Context();

    void sync() const;

    void close() const;

    void render(const std::vector<procedure_t> &body) const;

    void renderLoop(const condition_t &condition, const std::vector<procedure_t> &body) const;

    glm::ivec2 getWindowSize() const;

    void captureCursor(bool on = true) const;

    void setController(Controller *ctrl);

    Controller &getController();

private:
    void clear() const;

    void swap() const;

    glm::ivec2 _windowSize;
    Controller *_controller;
    GLFWwindow *_window;
};
