#pragma once


class Controller;


#include "context.hpp"
#include "math.hpp"


class Controller
{
public:
    // Callbacks
    void onMouseMove(glm::vec2 pos);
    void onMouseEvent(int button, int action);
    void onKeyEvent(int key, int action, int mods);

    // Get State
    glm::vec3 getDeltaRotation();;
    glm::vec3 getDeltaTranslation();
    bool shouldExit();
    bool shouldShoot();
    bool isAccelerating();
    void update();

private:
    // Misc state
    bool exit = false;
    bool accelerate = false;
    bool shoot = false;

    // Mouse location and unprocessed movement
    glm::vec2 mousePos;
    glm::vec2 mouseRot;

    // Unprocessed keyboard translation and rotation
    glm::vec3 keybdRot;
    glm::vec3 keybdTra;

    // Window size and settings
    float mouseLookSensitivity = 0.00125f;
    float keybdLookSensitivity = 0.025f;
    float keybdMoveSensitivity = 1.0f;
};
