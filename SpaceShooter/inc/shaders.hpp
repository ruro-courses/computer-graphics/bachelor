#pragma once

class Shader;

#include "context.hpp"
#include "math.hpp"

#include <unordered_map>


class Shader
{
public:
    explicit Shader(const std::string &name);
    Shader() = delete;
    ~Shader();

    void enable() const;
    void disable() const;

    void setUniform(const std::string &location, float value) const;
    void setUniform(const std::string &location, double value) const;
    void setUniform(const std::string &location, int value) const;
    void setUniform(const std::string &location, unsigned int value) const;
    void setUniform(const std::string &location, const float value[3]) const;
    void setUniform(const std::string &location, glm::vec3 value) const;
    void setUniform(const std::string &location, glm::ivec2 value) const;
    void setUniform(const std::string &location, glm::mat4x4 value) const;

private:
    void initialize();

    std::string shaderPath;
    GLuint shaderProgram;
    std::unordered_map<GLenum, std::string> shaderSources;
    std::unordered_map<GLenum, GLuint> shaderStages;
};
