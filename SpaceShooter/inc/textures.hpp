#pragma once


#include "context.hpp"

#include <unordered_map>


class Image
{
public:
    GLsizei w = 0, h = 0, size = 0;
    GLubyte *data = nullptr;

    bool compressed = false;
    GLenum storage_format = GL_RGBA8, loading_format = GL_BGRA;

    virtual ~Image() = default;
};


class SOILImage : public Image
{
public:
    explicit SOILImage(std::string name);

    ~SOILImage() override;
};


class DDSImage : public Image
{
private:
    int fd = -1;
    void *fmap = nullptr;
    std::size_t fsize = 0;

public:
    explicit DDSImage(std::string name);

    ~DDSImage() override;
};


using TextureParameters = std::unordered_map<GLenum, GLint>;


class Texture
{
public:
    explicit Texture(GLenum target, const Image &img, TextureParameters params, GLint _levels);

    virtual ~Texture();

    void bind(GLuint uint);

protected:
    GLint levels;
    GLuint name;
};


const TextureParameters _texture2DParameters = {
        {GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR},
        {GL_TEXTURE_MAG_FILTER, GL_LINEAR},
        {GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE},
        {GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE},
};


class Texture2D : public Texture
{
public:
    explicit Texture2D(const Image &img, TextureParameters params = _texture2DParameters, GLint _levels = 0);
};


const TextureParameters _cubemapParameters = {
        {GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR},
        {GL_TEXTURE_MAG_FILTER, GL_LINEAR},
        {GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE},
        {GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE},
        {GL_TEXTURE_WRAP_R,     GL_CLAMP_TO_EDGE},
};


class Cubemap : public Texture
{
public:
    explicit Cubemap(const Image &img, TextureParameters params = _cubemapParameters, GLint _levels = 0);
};


const TextureParameters _fontParameters = {
        {GL_TEXTURE_MIN_FILTER, GL_LINEAR},
        {GL_TEXTURE_MAG_FILTER, GL_LINEAR},
        {GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE},
        {GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE},
        {GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE},
};


class Font : public Texture
{
public:
    explicit Font(const Image &img, GLsizei chrs = 95, TextureParameters params = _fontParameters, GLint _levels = 0);
};
