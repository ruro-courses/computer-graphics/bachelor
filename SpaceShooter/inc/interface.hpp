#pragma once

struct LightData;
struct PlainVert;
struct ObjectVert;


#include "context.hpp"
#include "math.hpp"

#include <OBJ/tinyobj.hpp>


using field_t = struct { GLenum type; GLint count; GLuint offset; };
using layout_t = field_t[];
template<typename T> using conversion_t = T(const tinyobj::attrib_t &, const tinyobj::index_t &);
#define off(T, M) static_cast<GLuint>(offsetof(T, M))

// region LightData

struct LightData
{
    glm::vec4 pos;
    glm::vec4 col;
    static layout_t const layout;
};

constexpr layout_t const LightData::layout = {
        {GL_FLOAT, 4, off(LightData, pos)},
        {GL_FLOAT, 4, off(LightData, col)},
};
// endregion

// region TextData

struct TextData
{
    int ch;
    static layout_t const layout;
};

constexpr layout_t const TextData::layout = {
        {GL_INT, 1, off(TextData, ch)},
};
// endregion

// region PlainVert

struct PlainVert
{
    glm::vec3 pos;
    static layout_t const layout;
    static conversion_t<PlainVert> conversion;
};

constexpr layout_t const PlainVert::layout = {
        {GL_FLOAT, 3, off(PlainVert, pos)},
};
//endregion

// region ObjectVert

struct ObjectVert
{
    glm::vec3 pos;
    glm::vec3 nor;
    glm::vec2 uvs;
    static layout_t const layout;
    static conversion_t<ObjectVert> conversion;
};

constexpr layout_t const ObjectVert::layout = {
        {GL_FLOAT, 3, off(ObjectVert, pos)},
        {GL_FLOAT, 3, off(ObjectVert, nor)},
        {GL_FLOAT, 2, off(ObjectVert, uvs)},
};

// endregion ObjectVert
