#pragma once

class ResourceStore;

#include "shaders.hpp"
#include "textures.hpp"
#include "models.hpp"

#include <unordered_map>
#include <memory>


class ResourceStore
{
private:
    explicit ResourceStore() = default;
    ~ResourceStore() = default;

    std::unordered_map<std::string, std::unique_ptr<Texture>> _textures;
    std::unordered_map<std::string, std::unique_ptr<Shader>> _shaders;
    std::unordered_map<std::string, std::unique_ptr<ObjectModel>> _models;

    void loadTexture(const std::string &name);
    void loadShader(const std::string &name);
    void loadModel(const std::string &name);

    // This is a singleton (see private constructor)
    static auto &instance();

public:
    ResourceStore(const ResourceStore &) = delete;
    ResourceStore &operator=(const ResourceStore &) = delete;
    ResourceStore(ResourceStore &&) = delete;
    ResourceStore &operator=(ResourceStore &&) = delete;

    static Texture &getTexture(const std::string &name);
    static Shader &getShader(const std::string &name);
    static ObjectModel &getModel(const std::string &name);
};
