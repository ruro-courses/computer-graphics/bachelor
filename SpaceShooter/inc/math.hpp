#pragma once


#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <OBJ/tinyobj.hpp>


template<typename T, glm::qualifier Q>
glm::mat<4, 4, T, Q> tra_from_vector(glm::vec<3, T, Q> v) { return glm::translate(glm::mat<4, 4, T, Q>{1}, v); }

template<int S, typename T, glm::qualifier Q>
glm::vec<S, T, Q> safe_normalize(glm::vec<S, T, Q> v) { return (glm::length(v) > 0) ? glm::normalize(v) : glm::vec<S, T, Q>{0}; }

inline glm::vec3 safe_conversion3(const std::vector<tinyobj::real_t> &src, std::size_t offset)
{
    return 3 * offset < src.size()
           ? glm::make_vec3(&src[3 * offset])
           : glm::vec3(0, 0, 0);
}

inline glm::vec2 safe_conversion2(const std::vector<tinyobj::real_t> &src, std::size_t offset)
{
    return 2 * offset < src.size()
           ? glm::make_vec2(&src[2 * offset])
           : glm::vec2(0, 0);
}

