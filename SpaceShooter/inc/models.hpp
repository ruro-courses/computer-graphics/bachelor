#pragma once


class SkyboxModel;
class SkydustModel;
class ObjectModel;
class UIModel;


#include "entity.hpp"
#include "world.hpp"
#include "resources.hpp"
#include "buffers.hpp"

#include <OBJ/tinyobj.hpp>

class SkyboxModel
{
public:
    explicit SkyboxModel(const std::string &name, glm::vec3 subPosition);
    void draw();
    Shader &shader;
    glm::vec3 sun;
private:
    Texture &texture;
    VAO vao;
};

class SkydustModel
{
public:
    explicit SkydustModel(std::size_t num_particles);
    void draw();
    Shader &shader;
private:
    VAO vao;
};

class ObjectModel
{
public:
    explicit ObjectModel(const std::string &name);
    void draw(const std::vector<Entity> &M);
    void draw();
    float getRadius();
private:
    void drawStart(Shader &);
    void bindVAO(Shader &, size_t vidx);

    float radius = 0;
    std::vector<tinyobj::material_t> mats;
    std::vector<VAO> vaos;
};

class UIModel
{
public:
    explicit UIModel(const std::string &overlay = "overlay.dds", glm::ivec2 size = {30, 10});
    void draw();
    void setText(const std::vector<std::string> &_text);
    Shader &shader;

private:
    SSBO<TextData> text;
    glm::ivec2 size;
    Texture &font;
    Texture &overlay;
    VAO screen;
};
