#pragma once


#include "litemath.hpp"


struct ObjectData
{
    LiteMath::float4x4 modelMatrix;
    LiteMath::float4 dimensions;
    LiteMath::float4 color;
    LiteMath::int4 type;
};

struct LightData
{
    LiteMath::float4 location;
    LiteMath::float4 color;
};
