#pragma once


#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <string>
#include <functional>


#define GL_CHECK_ERRORS() ThrowExceptionOnGLError(__LINE__,__FILE__)


constexpr GLbitfield _defaultStorageMode = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT |
                                           GL_MAP_COHERENT_BIT | GL_DYNAMIC_STORAGE_BIT | GL_CLIENT_STORAGE_BIT;
constexpr GLenum _defaultMapMode = GL_READ_WRITE;


void registerCallbacks(
        GLFWwindow *window,
        GLFWcursorenterfun cursorEnterCallback = nullptr,
        GLFWwindowsizefun windowResizeCallback = nullptr,
        GLFWcursorposfun mouseMoveCallback = nullptr,
        GLFWkeyfun keyEventCallback = nullptr
);
std::function<double(void)> getFPScallback(GLFWwindow *window, const std::string &title, double updateInterval = 1);

GLuint makeSSBO(GLuint binding);
std::pair<GLuint, GLuint> makeVBOVAO(GLuint location, void *data, std::size_t Bsize, GLint Asize);
void *makeBuffer(std::size_t size, GLbitfield storageMode = _defaultStorageMode, GLenum mapMode = _defaultMapMode);

void clear();
void draw(GLFWwindow *window, int vertexCount);

GLFWwindow *init(int width, int height, const char *title, int v_major = 3, int v_minor = 3, int multisample = 0);
std::string getVendorString();
void ThrowExceptionOnGLError(int line, const char *file);
