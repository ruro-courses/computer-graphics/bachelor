#pragma once


#include <math.h>
#include <stdlib.h>
#include <memory>
#include <vector>
#include <iostream>

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

namespace LiteMath
{
    template<typename T> struct v2;
    template<typename T> struct v3;
    template<typename T> struct v4;

    template<typename T> struct v2
    {
        inline v2() noexcept : x(0), y(0) {}
        inline v2(T a, T b) noexcept : x(a), y(b) {}

        inline T *L() noexcept { return (T *) this; }
        inline const T *L() const noexcept { return (T *) this; }
        inline operator v3<T>() noexcept { return v3<T>(x, y, 1); }

        T x, y;
    };

    template<typename T> struct v3
    {
        inline v3() noexcept : x(0), y(0), z(0) {}
        inline v3(T a, T b, T c) noexcept : x(a), y(b), z(c) {}

        inline T *L() noexcept { return (T *) this; }
        inline const T *L() const noexcept { return (T *) this; }
        inline operator v4<T>() noexcept { return v4<T>(x, y, z, 1); }

        T x, y, z;
    };

    template<typename T> struct v4
    {
        inline v4() noexcept : x(0), y(0), z(0), w(0) {}
        inline v4(T a, T b, T c, T d) noexcept : x(a), y(b), z(c), w(d) {}

        inline T *L() noexcept { return (T *) this; }
        inline const T *L() const noexcept { return (T *) this; }

        T x, y, z, w;
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    using float2=v2<float>;
    using float3=v3<float>;
    using float4=v4<float>;

    using int2=v2<int>;
    using int3=v3<int>;
    using int4=v4<int>;

    using uchar4=v4<unsigned char>;

    using uint2=v2<unsigned int>;
    using uint4=v4<unsigned int>;

    using ushort2=v2<short>;
    using ushort4=v4<short>;

    struct float4x4;
    struct float3x3
    {
        inline float3x3() noexcept : row() { identity(); }
        inline float3x3(float3 a, float3 b, float3 c) noexcept : row()
        {
            row[0].x = a.x;
            row[1].x = a.y;
            row[2].x = a.z;

            row[0].y = b.x;
            row[1].y = b.y;
            row[2].y = b.z;

            row[0].z = c.x;
            row[1].z = c.y;
            row[2].z = c.z;
        }

        inline void identity() noexcept
        {
            row[0] = float3(1, 0, 0);
            row[1] = float3(0, 1, 0);
            row[2] = float3(0, 0, 1);
        }

        inline float &M(int x, int y) noexcept { return ((float *) row)[y * 3 + x]; }
        inline float M(int x, int y) const noexcept { return ((float *) row)[y * 3 + x]; }

        inline float *L() noexcept { return (float *) row; }
        inline const float *L() const noexcept { return (float *) row; }

        inline operator float4x4() noexcept;

        float3 row[3];
    };

    struct float4x4
    {
        inline float4x4() noexcept : row() { identity(); }

        inline explicit float4x4(const float arr[16]) noexcept : row()
        {
            row[0] = float4(arr[0], arr[1], arr[2], arr[3]);
            row[1] = float4(arr[4], arr[5], arr[6], arr[7]);
            row[2] = float4(arr[8], arr[9], arr[10], arr[11]);
            row[3] = float4(arr[12], arr[13], arr[14], arr[15]);
        }

        inline float4x4(float4 a, float4 b, float4 c, float4 d) noexcept : row()
        {
            row[0].x = a.x;
            row[1].x = a.y;
            row[2].x = a.z;
            row[3].x = a.w;

            row[0].y = b.x;
            row[1].y = b.y;
            row[2].y = b.z;
            row[3].y = b.w;

            row[0].z = c.x;
            row[1].z = c.y;
            row[2].z = c.z;
            row[3].z = c.w;

            row[0].w = d.x;
            row[1].w = d.y;
            row[2].w = d.z;
            row[3].w = d.w;
        }

        inline void identity() noexcept
        {
            row[0] = float4(1, 0, 0, 0);
            row[1] = float4(0, 1, 0, 0);
            row[2] = float4(0, 0, 1, 0);
            row[3] = float4(0, 0, 0, 1);
        }

        inline float &M(int x, int y) noexcept { return ((float *) row)[y * 4 + x]; }
        inline float M(int x, int y) const noexcept { return ((float *) row)[y * 4 + x]; }

        inline float *L() noexcept { return (float *) row; }
        inline const float *L() const noexcept { return (float *) row; }

        float4 row[4];
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    inline float rnd(float s, float e) noexcept
    {
        float t = (float) (rand()) / (float) RAND_MAX;
        return s + t * (e - s);
    }

    inline float clamp(float u, float a, float b) noexcept
    {
        float r = fmax(a, u);
        return fmin(r, b);
    }

    inline int clamp(int u, int a, int b) noexcept
    {
        int r = (a > u) ? a : u;
        return (r < b) ? r : b;
    }

    inline int max(int a, int b) noexcept { return a > b ? a : b; }
    inline int min(int a, int b) noexcept { return a < b ? a : b; }

    #define SQR(x) ((x)*(x))

    inline float2 to_float2(float4 v) noexcept { return {v.x, v.y}; }
    inline float2 to_float2(float3 v) noexcept { return {v.x, v.y}; }
    inline float3 to_float3(float4 v) noexcept { return {v.x, v.y, v.z}; }
    inline float3 to_float3(float2 v, float z = 1) noexcept { return {v.x, v.y, z}; }
    inline float4 to_float4(float3 v, float w = 1) noexcept { return {v.x, v.y, v.z, w}; }
    inline float4x4 to_float4x4(float3x3 v, float ww = 1) noexcept
    {
        return {
                {v.row[0].x, v.row[1].x, v.row[2].x, 0},
                {v.row[0].y, v.row[1].y, v.row[2].y, 0},
                {v.row[0].z, v.row[1].z, v.row[2].z, 0},
                {0,          0,          0,          ww},
        };
    }
    inline float3x3::operator float4x4() noexcept { return to_float4x4(*this); }

    inline float3x3 to_float3x3(float4x4 v) noexcept
    {
        return {
                {v.row[0].x, v.row[1].x, v.row[2].x},
                {v.row[0].y, v.row[1].y, v.row[2].y},
                {v.row[0].z, v.row[1].z, v.row[2].z},
        };
    }

    //**********************************************************************************
    // float4 operators and functions
    //**********************************************************************************
    inline float4 operator*(const float4 &u, float v) noexcept { return {u.x * v, u.y * v, u.z * v, u.w * v}; }
    inline float4 operator/(const float4 &u, float v) noexcept { return {u.x / v, u.y / v, u.z / v, u.w / v}; }
    inline float4 operator*(float v, const float4 &u) noexcept { return {v * u.x, v * u.y, v * u.z, v * u.w}; }
    inline float4 operator/(float v, const float4 &u) noexcept { return {v / u.x, v / u.y, v / u.z, v / u.w}; }

    inline float4 operator+(const float4 &u, const float4 &v) noexcept { return {u.x + v.x, u.y + v.y, u.z + v.z, u.w + v.w}; }
    inline float4 operator-(const float4 &u, const float4 &v) noexcept { return {u.x - v.x, u.y - v.y, u.z - v.z, u.w - v.w}; }
    inline float4 operator*(const float4 &u, const float4 &v) noexcept { return {u.x * v.x, u.y * v.y, u.z * v.z, u.w * v.w}; }
    inline float4 operator/(const float4 &u, const float4 &v) noexcept { return {u.x / v.x, u.y / v.y, u.z / v.z, u.w / v.w}; }

    inline float4 &operator+=(float4 &u, const float4 &v) noexcept
    {
        u.x += v.x;
        u.y += v.y;
        u.z += v.z;
        u.w += v.w;
        return u;
    }

    inline float4 &operator-=(float4 &u, const float4 &v) noexcept
    {
        u.x -= v.x;
        u.y -= v.y;
        u.z -= v.z;
        u.w -= v.w;
        return u;
    }

    inline float4 &operator*=(float4 &u, const float4 &v) noexcept
    {
        u.x *= v.x;
        u.y *= v.y;
        u.z *= v.z;
        u.w *= v.w;
        return u;
    }

    inline float4 &operator/=(float4 &u, const float4 &v) noexcept
    {
        u.x /= v.x;
        u.y /= v.y;
        u.z /= v.z;
        u.w /= v.w;
        return u;
    }

    inline float4 &operator+=(float4 &u, float v) noexcept
    {
        u.x += v;
        u.y += v;
        u.z += v;
        u.w += v;
        return u;
    }

    inline float4 &operator-=(float4 &u, float v) noexcept
    {
        u.x -= v;
        u.y -= v;
        u.z -= v;
        u.w -= v;
        return u;
    }

    inline float4 &operator*=(float4 &u, float v) noexcept
    {
        u.x *= v;
        u.y *= v;
        u.z *= v;
        u.w *= v;
        return u;
    }

    inline float4 &operator/=(float4 &u, float v) noexcept
    {
        u.x /= v;
        u.y /= v;
        u.z /= v;
        u.w /= v;
        return u;
    }

    inline float4 operator-(const float4 &v) noexcept { return {-v.x, -v.y, -v.z, -v.w}; }

    inline float4 catmullrom(const float4 &P0, const float4 &P1, const float4 &P2, const float4 &P3, float t) noexcept
    {
        const float ts = t * t;
        const float tc = t * ts;

        return (P0 * (-tc + 2.0f * ts - t) + P1 * (3.0f * tc - 5.0f * ts + 2.0f) + P2 * (-3.0f * tc + 4.0f * ts + t) + P3 * (tc - ts)) * 0.5f;
    }

    inline float4 lerp(const float4 &u, const float4 &v, float t) noexcept { return u + t * (v - u); }
    inline float dot(const float4 &u, const float4 &v) noexcept { return (u.x * v.x + u.y * v.y + u.z * v.z + u.w * v.w); }
    inline float dot3(const float4 &u, const float4 &v) noexcept { return (u.x * v.x + u.y * v.y + u.z * v.z); }
    inline float dot3(const float4 &u, const float3 &v) noexcept { return (u.x * v.x + u.y * v.y + u.z * v.z); }

    inline float4 clamp(const float4 &u, float a, float b) noexcept
    {
        return {
                clamp(u.x, a, b),
                clamp(u.y, a, b),
                clamp(u.z, a, b),
                clamp(u.w, a, b)
        };
    }

    inline float length3(const float4 &u) noexcept { return sqrtf(SQR(u.x) + SQR(u.y) + SQR(u.z)); }
    inline float length(const float4 &u) noexcept { return sqrtf(SQR(u.x) + SQR(u.y) + SQR(u.z) + SQR(u.w)); }

    //**********************************************************************************
    // float3 operators and functions
    //**********************************************************************************
    inline float3 operator*(const float3 &u, float v) noexcept { return {u.x * v, u.y * v, u.z * v}; }
    inline float3 operator/(const float3 &u, float v) noexcept { return {u.x / v, u.y / v, u.z / v}; }
    inline float3 operator*(float v, const float3 &u) noexcept { return {v * u.x, v * u.y, v * u.z}; }
    inline float3 operator/(float v, const float3 &u) noexcept { return {v / u.x, v / u.y, v / u.z}; }

    inline float3 operator+(const float3 &u, const float3 &v) noexcept { return {u.x + v.x, u.y + v.y, u.z + v.z}; }
    inline float3 operator-(const float3 &u, const float3 &v) noexcept { return {u.x - v.x, u.y - v.y, u.z - v.z}; }
    inline float3 operator*(const float3 &u, const float3 &v) noexcept { return {u.x * v.x, u.y * v.y, u.z * v.z}; }
    inline float3 operator/(const float3 &u, const float3 &v) noexcept { return {u.x / v.x, u.y / v.y, u.z / v.z}; }

    inline float3 operator-(const float3 &u) noexcept { return {-u.x, -u.y, -u.z}; }

    inline float3 &operator+=(float3 &u, const float3 &v) noexcept
    {
        u.x += v.x;
        u.y += v.y;
        u.z += v.z;
        return u;
    }

    inline float3 &operator-=(float3 &u, const float3 &v) noexcept
    {
        u.x -= v.x;
        u.y -= v.y;
        u.z -= v.z;
        return u;
    }

    inline float3 &operator*=(float3 &u, const float3 &v) noexcept
    {
        u.x *= v.x;
        u.y *= v.y;
        u.z *= v.z;
        return u;
    }

    inline float3 &operator/=(float3 &u, const float3 &v) noexcept
    {
        u.x /= v.x;
        u.y /= v.y;
        u.z /= v.z;
        return u;
    }

    inline float3 &operator+=(float3 &u, float v) noexcept
    {
        u.x += v;
        u.y += v;
        u.z += v;
        return u;
    }

    inline float3 &operator-=(float3 &u, float v) noexcept
    {
        u.x -= v;
        u.y -= v;
        u.z -= v;
        return u;
    }

    inline float3 &operator*=(float3 &u, float v) noexcept
    {
        u.x *= v;
        u.y *= v;
        u.z *= v;
        return u;
    }

    inline float3 &operator/=(float3 &u, float v) noexcept
    {
        u.x /= v;
        u.y /= v;
        u.z /= v;
        return u;
    }


    inline float3 catmullrom(const float3 &P0, const float3 &P1, const float3 &P2, const float3 &P3, float t) noexcept
    {
        const float ts = t * t;
        const float tc = t * ts;

        return (P0 * (-tc + 2.0f * ts - t) + P1 * (3.0f * tc - 5.0f * ts + 2.0f) + P2 * (-3.0f * tc + 4.0f * ts + t) + P3 * (tc - ts)) * 0.5f;
    }

    inline float3 lerp(const float3 &u, const float3 &v, float t) noexcept { return u + t * (v - u); }
    inline float dot(const float3 &u, const float3 &v) noexcept { return (u.x * v.x + u.y * v.y + u.z * v.z); }
    inline float3 cross(const float3 &u, const float3 &v) noexcept
    {
        return {
                u.y * v.z - u.z * v.y,
                u.z * v.x - u.x * v.z,
                u.x * v.y - u.y * v.x
        };
    }

    inline float3 clamp(const float3 &u, float a, float b) noexcept { return {clamp(u.x, a, b), clamp(u.y, a, b), clamp(u.z, a, b)}; }

    inline float triple(const float3 &a, const float3 &b, const float3 &c) noexcept { return dot(a, cross(b, c)); }
    inline float length(const float3 &u) noexcept { return sqrtf(SQR(u.x) + SQR(u.y) + SQR(u.z)); }
    inline float lengthSquare(const float3 u) noexcept { return u.x * u.x + u.y * u.y + u.z * u.z; }
    inline float3 normalize0(const float3 &u) noexcept
    {
        float lu = length(u);
        return lu > 0 ? u / lu : u;
    }
    inline float3 normalize(const float3 &u) noexcept { return u / length(u); }
    inline float coordSumm(const float3 u) noexcept { return u.x * +u.y + u.z; }

    inline float maxcomp(const float3 &u) noexcept { return fmax(u.x, fmax(u.y, u.z)); }
    inline float mincomp(const float3 &u) noexcept { return fmin(u.x, fmin(u.y, u.z)); }


    //**********************************************************************************
    // float2 operators and functions
    //**********************************************************************************

    inline float2 operator*(const float2 &u, float v) noexcept { return {u.x * v, u.y * v}; }
    inline float2 operator/(const float2 &u, float v) noexcept { return {u.x / v, u.y / v}; }
    inline float2 operator*(float v, const float2 &u) noexcept { return {v * u.x, v * u.y}; }
    inline float2 operator/(float v, const float2 &u) noexcept { return {v / u.x, v / u.y}; }

    inline float2 operator+(const float2 &u, const float2 &v) noexcept { return {u.x + v.x, u.y + v.y}; }
    inline float2 operator-(const float2 &u, const float2 &v) noexcept { return {u.x - v.x, u.y - v.y}; }
    inline float2 operator*(const float2 &u, const float2 &v) noexcept { return {u.x * v.x, u.y * v.y}; }
    inline float2 operator/(const float2 &u, const float2 &v) noexcept { return {u.x / v.x, u.y / v.y}; }

    inline float2 operator-(const float2 &v) noexcept { return {-v.x, -v.y}; }

    inline float2 &operator+=(float2 &u, const float2 &v) noexcept
    {
        u.x += v.x;
        u.y += v.y;
        return u;
    }

    inline float2 &operator-=(float2 &u, const float2 &v) noexcept
    {
        u.x -= v.x;
        u.y -= v.y;
        return u;
    }

    inline float2 &operator*=(float2 &u, const float2 &v) noexcept
    {
        u.x *= v.x;
        u.y *= v.y;
        return u;
    }

    inline float2 &operator/=(float2 &u, const float2 &v) noexcept
    {
        u.x /= v.x;
        u.y /= v.y;
        return u;
    }

    inline float2 &operator+=(float2 &u, float v) noexcept
    {
        u.x += v;
        u.y += v;
        return u;
    }

    inline float2 &operator-=(float2 &u, float v) noexcept
    {
        u.x -= v;
        u.y -= v;
        return u;
    }

    inline float2 &operator*=(float2 &u, float v) noexcept
    {
        u.x *= v;
        u.y *= v;
        return u;
    }

    inline float2 &operator/=(float2 &u, float v) noexcept
    {
        u.x /= v;
        u.y /= v;
        return u;
    }

    inline float2 catmullrom(const float2 &P0, const float2 &P1, const float2 &P2, const float2 &P3, float t) noexcept
    {
        const float ts = t * t;
        const float tc = t * ts;

        return (P0 * (-tc + 2.0f * ts - t) + P1 * (3.0f * tc - 5.0f * ts + 2.0f) + P2 * (-3.0f * tc + 4.0f * ts + t) + P3 * (tc - ts)) * 0.5f;
    }

    inline float2 lerp(const float2 &u, const float2 &v, float t) noexcept { return u + t * (v - u); }
    inline float dot(const float2 &u, const float2 &v) noexcept { return (u.x * v.x + u.y * v.y); }
    inline float2 clamp(const float2 &u, float a, float b) noexcept { return {clamp(u.x, a, b), clamp(u.y, a, b)}; }

    inline float length(const float2 &u) noexcept { return sqrtf(SQR(u.x) + SQR(u.y)); }
    inline float2 normalize(const float2 &u) noexcept { return u / length(u); }

    inline float lerp(float u, float v, float t) noexcept { return u + t * (v - u); }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    inline bool IntersectBoxBox(float2 box1Min, float2 box1Max, float2 box2Min, float2 box2Max) noexcept
    {
        return box1Min.x <= box2Max.x && box2Min.x <= box1Max.x &&
               box1Min.y <= box2Max.y && box2Min.y <= box1Max.y;
    }

    inline bool IntersectBoxBox(int2 box1Min, int2 box1Max, int2 box2Min, int2 box2Max) noexcept
    {
        return box1Min.x <= box2Max.x && box2Min.x <= box1Max.x &&
               box1Min.y <= box2Max.y && box2Min.y <= box1Max.y;
    }

    inline float4 operator*(const float4x4 &m, const float4 &v) noexcept
    {
        return {
                m.row[0].x * v.x + m.row[0].y * v.y + m.row[0].z * v.z + m.row[0].w * v.w,
                m.row[1].x * v.x + m.row[1].y * v.y + m.row[1].z * v.z + m.row[1].w * v.w,
                m.row[2].x * v.x + m.row[2].y * v.y + m.row[2].z * v.z + m.row[2].w * v.w,
                m.row[3].x * v.x + m.row[3].y * v.y + m.row[3].z * v.z + m.row[3].w * v.w
        };
    }

    inline float3 operator*(const float3x3 &m, const float3 &v) noexcept
    {
        return {
                m.row[0].x * v.x + m.row[0].y * v.y + m.row[0].z * v.z,
                m.row[1].x * v.x + m.row[1].y * v.y + m.row[1].z * v.z,
                m.row[2].x * v.x + m.row[2].y * v.y + m.row[2].z * v.z
        };
    }

    inline float4x4 operator*(const float4x4 &m1, const float4x4 &m2) noexcept
    {
        return {
                m1 * float4(m2.row[0].x, m2.row[1].x, m2.row[2].x, m2.row[3].x),
                m1 * float4(m2.row[0].y, m2.row[1].y, m2.row[2].y, m2.row[3].y),
                m1 * float4(m2.row[0].z, m2.row[1].z, m2.row[2].z, m2.row[3].z),
                m1 * float4(m2.row[0].w, m2.row[1].w, m2.row[2].w, m2.row[3].w),
        };
    }

    inline float3x3 operator*(const float3x3 &m1, const float3x3 &m2) noexcept
    {
        return {
                m1 * float3(m2.row[0].x, m2.row[1].x, m2.row[2].x),
                m1 * float3(m2.row[0].y, m2.row[1].y, m2.row[2].y),
                m1 * float3(m2.row[0].z, m2.row[1].z, m2.row[2].z),
        };
    }

    inline float4x4 transpose(const float4x4 &m) noexcept { return {m.row[0], m.row[1], m.row[2], m.row[3]}; }
    inline float3x3 transpose(const float3x3 &m) noexcept { return {m.row[0], m.row[1], m.row[2]}; }

    inline float4x4 translate(float3 t) noexcept
    {
        return {
                {1.0f, 0.0f, 0.0f, 0.0f},
                {0.0f, 1.0f, 0.0f, 0.0f},
                {0.0f, 0.0f, 1.0f, 0.0f},
                {t.x,  t.y,  t.z,  1.0f},
        };
    }

    inline float3x3 scale(float3 t) noexcept
    {
        return {
                {t.x,  0.0f, 0.0f},
                {0.0f, t.y,  0.0f},
                {0.0f, 0.0f, t.z},
        };
    }

    inline float3x3 rotate(float3 euler) noexcept
    {
        float sx = sin(euler.x), sy = sin(euler.y), sz = sin(euler.z);
        float cx = cos(euler.x), cy = cos(euler.y), cz = cos(euler.z);
        return {
                {cy * cz,  sx * sy * cz + sz * cx,  sx * sz - sy * cx * cz},
                {-sz * cy, -sx * sy * sz + cx * cz, sx * cz + sy * sz * cx},
                {sy,       -sx * cy,                cx * cy}
        };
    }

    inline float3x3 rotate_X(float alpha) noexcept
    {
        return {
                {1.0f, 0.0f,        0.0f},
                {0.0f, +cos(alpha), +sin(alpha)},
                {0.0f, -sin(alpha), +cos(alpha)},
        };
    }

    inline float3x3 rotate_Y(float beta) noexcept
    {
        return {
                {+cos(beta), 0.0f, -sin(beta)},
                {0.0f,       1.0f, 0.0f,},
                {+sin(beta), 0.0f, +cos(beta)},
        };
    }

    inline float3x3 rotate_Z(float gamma) noexcept
    {
        return {
                {+cos(gamma), sin(gamma), 0.0f},
                {-sin(gamma), cos(gamma), 0.0f},
                {0.0f,        0.0f,       1.0f},
        };
    }

    inline float4x4 inverse(float4x4 m1) noexcept
    {
        float tmp[12]; // temp array for pairs
        float4x4 m;

        // calculate pairs for first 8 elements (cofactors)
        //
        tmp[0] = m1.row[2].z * m1.row[3].w;
        tmp[1] = m1.row[2].w * m1.row[3].z;
        tmp[2] = m1.row[2].y * m1.row[3].w;
        tmp[3] = m1.row[2].w * m1.row[3].y;
        tmp[4] = m1.row[2].y * m1.row[3].z;
        tmp[5] = m1.row[2].z * m1.row[3].y;
        tmp[6] = m1.row[2].x * m1.row[3].w;
        tmp[7] = m1.row[2].w * m1.row[3].x;
        tmp[8] = m1.row[2].x * m1.row[3].z;
        tmp[9] = m1.row[2].z * m1.row[3].x;
        tmp[10] = m1.row[2].x * m1.row[3].y;
        tmp[11] = m1.row[2].y * m1.row[3].x;

        // calculate first 8 m1.rowents (cofactors)
        //
        m.row[0].x = tmp[0] * m1.row[1].y + tmp[3] * m1.row[1].z + tmp[4] * m1.row[1].w;
        m.row[0].x -= tmp[1] * m1.row[1].y + tmp[2] * m1.row[1].z + tmp[5] * m1.row[1].w;
        m.row[1].x = tmp[1] * m1.row[1].x + tmp[6] * m1.row[1].z + tmp[9] * m1.row[1].w;
        m.row[1].x -= tmp[0] * m1.row[1].x + tmp[7] * m1.row[1].z + tmp[8] * m1.row[1].w;
        m.row[2].x = tmp[2] * m1.row[1].x + tmp[7] * m1.row[1].y + tmp[10] * m1.row[1].w;
        m.row[2].x -= tmp[3] * m1.row[1].x + tmp[6] * m1.row[1].y + tmp[11] * m1.row[1].w;
        m.row[3].x = tmp[5] * m1.row[1].x + tmp[8] * m1.row[1].y + tmp[11] * m1.row[1].z;
        m.row[3].x -= tmp[4] * m1.row[1].x + tmp[9] * m1.row[1].y + tmp[10] * m1.row[1].z;
        m.row[0].y = tmp[1] * m1.row[0].y + tmp[2] * m1.row[0].z + tmp[5] * m1.row[0].w;
        m.row[0].y -= tmp[0] * m1.row[0].y + tmp[3] * m1.row[0].z + tmp[4] * m1.row[0].w;
        m.row[1].y = tmp[0] * m1.row[0].x + tmp[7] * m1.row[0].z + tmp[8] * m1.row[0].w;
        m.row[1].y -= tmp[1] * m1.row[0].x + tmp[6] * m1.row[0].z + tmp[9] * m1.row[0].w;
        m.row[2].y = tmp[3] * m1.row[0].x + tmp[6] * m1.row[0].y + tmp[11] * m1.row[0].w;
        m.row[2].y -= tmp[2] * m1.row[0].x + tmp[7] * m1.row[0].y + tmp[10] * m1.row[0].w;
        m.row[3].y = tmp[4] * m1.row[0].x + tmp[9] * m1.row[0].y + tmp[10] * m1.row[0].z;
        m.row[3].y -= tmp[5] * m1.row[0].x + tmp[8] * m1.row[0].y + tmp[11] * m1.row[0].z;

        // calculate pairs for second 8 m1.rowents (cofactors)
        //
        tmp[0] = m1.row[0].z * m1.row[1].w;
        tmp[1] = m1.row[0].w * m1.row[1].z;
        tmp[2] = m1.row[0].y * m1.row[1].w;
        tmp[3] = m1.row[0].w * m1.row[1].y;
        tmp[4] = m1.row[0].y * m1.row[1].z;
        tmp[5] = m1.row[0].z * m1.row[1].y;
        tmp[6] = m1.row[0].x * m1.row[1].w;
        tmp[7] = m1.row[0].w * m1.row[1].x;
        tmp[8] = m1.row[0].x * m1.row[1].z;
        tmp[9] = m1.row[0].z * m1.row[1].x;
        tmp[10] = m1.row[0].x * m1.row[1].y;
        tmp[11] = m1.row[0].y * m1.row[1].x;

        // calculate second 8 m1 (cofactors)
        //
        m.row[0].z = tmp[0] * m1.row[3].y + tmp[3] * m1.row[3].z + tmp[4] * m1.row[3].w;
        m.row[0].z -= tmp[1] * m1.row[3].y + tmp[2] * m1.row[3].z + tmp[5] * m1.row[3].w;
        m.row[1].z = tmp[1] * m1.row[3].x + tmp[6] * m1.row[3].z + tmp[9] * m1.row[3].w;
        m.row[1].z -= tmp[0] * m1.row[3].x + tmp[7] * m1.row[3].z + tmp[8] * m1.row[3].w;
        m.row[2].z = tmp[2] * m1.row[3].x + tmp[7] * m1.row[3].y + tmp[10] * m1.row[3].w;
        m.row[2].z -= tmp[3] * m1.row[3].x + tmp[6] * m1.row[3].y + tmp[11] * m1.row[3].w;
        m.row[3].z = tmp[5] * m1.row[3].x + tmp[8] * m1.row[3].y + tmp[11] * m1.row[3].z;
        m.row[3].z -= tmp[4] * m1.row[3].x + tmp[9] * m1.row[3].y + tmp[10] * m1.row[3].z;
        m.row[0].w = tmp[2] * m1.row[2].z + tmp[5] * m1.row[2].w + tmp[1] * m1.row[2].y;
        m.row[0].w -= tmp[4] * m1.row[2].w + tmp[0] * m1.row[2].y + tmp[3] * m1.row[2].z;
        m.row[1].w = tmp[8] * m1.row[2].w + tmp[0] * m1.row[2].x + tmp[7] * m1.row[2].z;
        m.row[1].w -= tmp[6] * m1.row[2].z + tmp[9] * m1.row[2].w + tmp[1] * m1.row[2].x;
        m.row[2].w = tmp[6] * m1.row[2].y + tmp[11] * m1.row[2].w + tmp[3] * m1.row[2].x;
        m.row[2].w -= tmp[10] * m1.row[2].w + tmp[2] * m1.row[2].x + tmp[7] * m1.row[2].y;
        m.row[3].w = tmp[10] * m1.row[2].z + tmp[4] * m1.row[2].x + tmp[9] * m1.row[2].y;
        m.row[3].w -= tmp[8] * m1.row[2].y + tmp[11] * m1.row[2].z + tmp[5] * m1.row[2].x;

        // calculate matrix inverse
        //
        float k = 1.0f / (m1.row[0].x * m.row[0].x + m1.row[0].y * m.row[1].x + m1.row[0].z * m.row[2].x + m1.row[0].w * m.row[3].x);

        m.row[0].x *= k;
        m.row[0].y *= k;
        m.row[0].z *= k;
        m.row[0].w *= k;

        m.row[1].x *= k;
        m.row[1].y *= k;
        m.row[1].z *= k;
        m.row[1].w *= k;

        m.row[2].x *= k;
        m.row[2].y *= k;
        m.row[2].z *= k;
        m.row[2].w *= k;

        m.row[3].x *= k;
        m.row[3].y *= k;
        m.row[3].z *= k;
        m.row[3].w *= k;

        return m;
    }

    // Look At matrix creation
    // return the transposed view matrix
    //
    inline float4x4 lookAtTransposed(float3 eye, float3 center, float3 up) noexcept
    {
        float3 x, y, z; // basis; will make a rotation matrix

        z.x = eye.x - center.x;
        z.y = eye.y - center.y;
        z.z = eye.z - center.z;
        z = normalize(z);

        y.x = up.x;
        y.y = up.y;
        y.z = up.z;

        x = cross(y, z); // X vector = Y cross Z
        y = cross(z, x); // Recompute Y = Z cross X

        // cross product gives area of parallelogram, which is < 1.0 for
        // non-perpendicular unit-length vectors; so normalize x, y here
        x = normalize(x);
        y = normalize(y);

        float4x4 M;
        M.row[0].x = x.x;
        M.row[1].x = x.y;
        M.row[2].x = x.z;
        M.row[3].x = -x.x * eye.x - x.y * eye.y - x.z * eye.z;
        M.row[0].y = y.x;
        M.row[1].y = y.y;
        M.row[2].y = y.z;
        M.row[3].y = -y.x * eye.x - y.y * eye.y - y.z * eye.z;
        M.row[0].z = z.x;
        M.row[1].z = z.y;
        M.row[2].z = z.z;
        M.row[3].z = -z.x * eye.x - z.y * eye.y - z.z * eye.z;
        M.row[0].w = 0.0;
        M.row[1].w = 0.0;
        M.row[2].w = 0.0;
        M.row[3].w = 1.0;
        return M;
    }

    inline void _my_frustumf3(float *matrix, float left, float right, float bottom, float top, float znear, float zfar) noexcept
    {
        float temp, temp2, temp3, temp4;
        temp = 2.0f * znear;
        temp2 = right - left;
        temp3 = top - bottom;
        temp4 = zfar - znear;
        matrix[0] = temp / temp2;
        matrix[1] = 0.0;
        matrix[2] = 0.0;
        matrix[3] = 0.0;
        matrix[4] = 0.0;
        matrix[5] = temp / temp3;
        matrix[6] = 0.0;
        matrix[7] = 0.0;
        matrix[8] = (right + left) / temp2;
        matrix[9] = (top + bottom) / temp3;
        matrix[10] = (-zfar - znear) / temp4;
        matrix[11] = -1.0f;
        matrix[12] = 0.0;
        matrix[13] = 0.0;
        matrix[14] = (-temp * zfar) / temp4;
        matrix[15] = 0.0;
    }

    inline float4x4 projectionMatrixTransposed(float fovy, float aspect, float zNear, float zFar) noexcept
    {
        float4x4 res;
        const float ymax = zNear * tanf(fovy * 3.14159265358979323846f / 360.0f);
        const float xmax = ymax * aspect;
        _my_frustumf3(res.L(), -xmax, xmax, -ymax, ymax, zNear, zFar);
        return res;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    inline float3 HSV2RGB(const float3 &HSV) noexcept
    {
        float hue = fmod(HSV.x, 360.0f) / 360.0f;
        float sat = clamp(HSV.y, 0.0f, 100.0f) / 100.0f;
        float val = clamp(HSV.z, 0.0f, 100.0f) / 100.0f;

        if (sat <= 0.0) { return {val, val, val}; }

        float angle = 6 * hue;
        int segment = int(angle);
        angle -= segment;

        float p = val * (1.0f - sat);
        float q = val * (1.0f - sat * angle);
        float t = val * (1.0f - sat * (1.0f - angle));

        switch (segment)
        {
            case 0: return {val, t, p};
            case 1: return {q, val, p};
            case 2: return {p, val, t};
            case 3: return {p, q, val};
            case 4: return {t, p, val};
            case 5:
            default: return {val, p, q};
        }
    }

    inline float3 HEX2RGB(const std::string &HEX) noexcept
    {
        const unsigned long hex = std::stoul(HEX, nullptr, 16);
        return float3(
                {
                        ((hex >> 16) & 0xFF) / 255.0f,
                        ((hex >> 8) & 0xFF) / 255.0f,
                        (hex & 0xFF) / 255.0f
                }
        );
    }

    inline int RealColorToUint32_BGRA(float4 real_color) noexcept
    {
        const float r = real_color.x * 255.0f;
        const float g = real_color.y * 255.0f;
        const float b = real_color.z * 255.0f;
        const float a = real_color.w * 255.0f;

        const auto red = (unsigned char) r;
        const auto green = (unsigned char) g;
        const auto blue = (unsigned char) b;
        const auto alpha = (unsigned char) a;

        return blue | (green << 8) | (red << 16) | (alpha << 24);
    }

    inline int RealColorToUint32_RGBA(float4 real_color) noexcept
    {
        const float r = real_color.x * 255.0f;
        const float g = real_color.y * 255.0f;
        const float b = real_color.z * 255.0f;
        const float a = real_color.w * 255.0f;

        const auto red = (unsigned char) r;
        const auto green = (unsigned char) g;
        const auto blue = (unsigned char) b;
        const auto alpha = (unsigned char) a;

        return red | (green << 8) | (blue << 16) | (alpha << 24);
    }

    static constexpr float DEG_TO_RAD = 3.1415926535897932384626433832795f / 180.0f;

    //////////////////////////

    static constexpr std::string_view vbegin = "[";
    static constexpr std::string_view vsep = ", ";
    static constexpr std::string_view vend = "]";

    template<typename T>
    inline std::ostream &operator<<(std::ostream &os, const v2<T> &dt)
    {
        os << vbegin;
        os << dt.x;
        os << vsep;
        os << dt.y;
        os << vend;
        return os;
    }

    template<typename T>
    inline std::ostream &operator<<(std::ostream &os, const v3<T> &dt)
    {
        os << vbegin;
        os << dt.x;
        os << vsep;
        os << dt.y;
        os << vsep;
        os << dt.z;
        os << vend;
        return os;
    }

    template<typename T>
    inline std::ostream &operator<<(std::ostream &os, const v4<T> &dt)
    {
        os << vbegin;
        os << dt.x;
        os << vsep;
        os << dt.y;
        os << vsep;
        os << dt.z;
        os << vsep;
        os << dt.w;
        os << vend;
        return os;
    }

    static constexpr std::string_view mbegin = "[\n";
    static constexpr std::string_view mindent = "    ";
    static constexpr std::string_view msep = ",\n";
    static constexpr std::string_view mend = "]";

    inline std::ostream &operator<<(std::ostream &os, const float4x4 &dt)
    {
        os << mbegin;
        os << mindent;
        os << dt.row[0];
        os << msep;
        os << mindent;
        os << dt.row[1];
        os << msep;
        os << mindent;
        os << dt.row[2];
        os << msep;
        os << mindent;
        os << dt.row[3];
        os << msep;
        os << mend;
        return os;
    }

}; // namespace LiteMath
