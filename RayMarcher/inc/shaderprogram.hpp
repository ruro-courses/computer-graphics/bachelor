#pragma once


#include <unordered_map>

#include "common.hpp"
#include "litemath.hpp"


class ShaderProgram
{
public:

    ShaderProgram() = delete;
    explicit ShaderProgram(const std::unordered_map<GLenum, std::string> &inputShaders, bool validate = false);
    virtual ~ShaderProgram() = default;
    void Release();
    void Reload();
    void StartUseShader() const;
    void StopUseShader() const;

    void SetDefine(const std::string &name, int value);
    void ResetDefines();

    void SetUniform(const std::string &location, float value) const;
    void SetUniform(const std::string &location, double value) const;
    void SetUniform(const std::string &location, int value) const;
    void SetUniform(const std::string &location, unsigned int value) const;
    void SetUniform(const std::string &location, LiteMath::int2) const;
    void SetUniform(const std::string &location, LiteMath::float3) const;
    void SetUniform(const std::string &location, LiteMath::float4x4) const;
private:
    void Compile();
    void Link();
    GLint GetUniformLocation(const std::string &location) const;
    GLuint LoadShaderObject(GLenum type, const std::string &filename);
    GLuint shaderProgram;

    bool enableValidation;
    std::unordered_map<GLenum, std::string> shaderSources;
    std::unordered_map<std::string, int> defines;
    std::unordered_map<GLenum, GLuint> shaderObjects;
};
