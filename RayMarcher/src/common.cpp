#include <iostream>
#include <fstream>
#include <sstream>

#include "common.hpp"


void registerCallbacks(
        GLFWwindow *window,
        GLFWcursorenterfun cursorEnterCallback,
        GLFWwindowsizefun windowResizeCallback,
        GLFWcursorposfun mouseMoveCallback,
        GLFWkeyfun keyEventCallback
)
{
    if (cursorEnterCallback) { glfwSetCursorEnterCallback(window, cursorEnterCallback); }

    if (windowResizeCallback)
    {
        glfwSetWindowSizeCallback(window, windowResizeCallback);
        int width, height;
        glfwGetWindowSize(window, &width, &height);
        windowResizeCallback(window, width, height);
    }

    if (mouseMoveCallback)
    {
        glfwSetCursorPosCallback(window, mouseMoveCallback);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    if (keyEventCallback) { glfwSetKeyCallback(window, keyEventCallback); }
}

std::function<double(void)> getFPScallback(GLFWwindow *window, const std::string &title, double updateInterval)
{
    return [window, title, updateInterval](){
        static int frames = 0;
        static double meanFPS = -1;
        static double last = glfwGetTime();

        double next = glfwGetTime();
        frames++;

        if (next - last >= updateInterval)
        {
            meanFPS = meanFPS < 0 ? frames : (meanFPS + frames) / 2;
            std::ostringstream fps_string;
            fps_string << title << " [" << meanFPS << " fps]";
            glfwSetWindowTitle(window, fps_string.str().c_str());

            last = next;
            frames = 0;
        }

        return meanFPS;
    };
}

GLuint makeSSBO(GLuint binding)
{
    GLuint SSBO;
    glGenBuffers(1, &SSBO);
    GL_CHECK_ERRORS();
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBO);
    GL_CHECK_ERRORS();
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, binding, SSBO);
    GL_CHECK_ERRORS();
    return SSBO;
}

std::pair<GLuint,GLuint> makeVBOVAO(GLuint location, void *data, std::size_t Bsize, GLint Asize)
{
    GLuint VBO, VAO;
    glGenBuffers(1, &VBO);
    GL_CHECK_ERRORS();
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    GL_CHECK_ERRORS();
    glBufferData(GL_ARRAY_BUFFER, Bsize, data, GL_STATIC_DRAW);
    GL_CHECK_ERRORS();

    glGenVertexArrays(1, &VAO);
    GL_CHECK_ERRORS();
    glBindVertexArray(VAO);
    GL_CHECK_ERRORS();

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    GL_CHECK_ERRORS();
    glEnableVertexAttribArray(location);
    GL_CHECK_ERRORS();
    glVertexAttribPointer(location, Asize, GL_FLOAT, GL_FALSE, 0, nullptr);
    GL_CHECK_ERRORS();

    glBindVertexArray(VAO);
    GL_CHECK_ERRORS();

    return std::pair(VBO, VAO);
}

void *makeBuffer(std::size_t size, GLbitfield storageMode, GLenum mapMode)
{
    glBufferStorage(GL_SHADER_STORAGE_BUFFER, size, nullptr, storageMode);
    GL_CHECK_ERRORS();
    void *buffer = glMapBuffer(GL_SHADER_STORAGE_BUFFER, mapMode);
    GL_CHECK_ERRORS();
    return buffer;
}

void clear()
{
    glClearColor(0, 0, 0, 1);
    GL_CHECK_ERRORS();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    GL_CHECK_ERRORS();
}

void draw(GLFWwindow *window, int vertexCount)
{
    glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexCount);
    GL_CHECK_ERRORS();

    // Ensure the mapped buffers were fully updated
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    GL_CHECK_ERRORS();
    glFinish();
    GL_CHECK_ERRORS();
    glfwSwapBuffers(window);
    // Ensure the rendering was completed and buffers were pulled
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    GL_CHECK_ERRORS();
    glFinish();
    GL_CHECK_ERRORS();
}

GLFWwindow *init(int width, int height, const char *title, int v_major, int v_minor, int multisample)
{
    if (!glfwInit()) { throw std::runtime_error("Failed to initialize GLFW context"); }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, v_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, v_minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    if (multisample) {
        std::cout << "Using " << multisample << "xMSAA" << std::endl;
        glfwWindowHint(GLFW_SAMPLES, multisample);
    }

    GLFWwindow *window = glfwCreateWindow(width, height, title, nullptr, nullptr);
    if (window == nullptr)
    {
        glfwTerminate();
        throw std::runtime_error(
                "Failed to create GLFW window. Notice, that OpenGL version " +
                std::to_string(v_major) + "." + std::to_string(v_minor) +
                " or higher is required for this application."
        );
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) { throw std::runtime_error("Failed to initialize OpenGL context"); }

    return window;
}


std::string getVendorString()
{
    std::ostringstream vString;

    vString << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
    vString << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
    vString << "Version: " << glGetString(GL_VERSION) << std::endl;
    vString << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

    return vString.str();
}


#define ERR_CASE(ERR, MSG)  \
    case ERR:               \
        MSG += #ERR;        \
        break


void ThrowExceptionOnGLError(int line, const char *file)
{
    std::string errMsg;

    GLenum gl_error = glGetError();

    switch (gl_error)
    {
        ERR_CASE(GL_INVALID_ENUM, errMsg);
        ERR_CASE(GL_INVALID_VALUE, errMsg);
        ERR_CASE(GL_INVALID_OPERATION, errMsg);
        ERR_CASE(GL_STACK_OVERFLOW, errMsg);
        ERR_CASE(GL_STACK_UNDERFLOW, errMsg);
        ERR_CASE(GL_OUT_OF_MEMORY, errMsg);
        case GL_NO_ERROR:return;

        default:errMsg += "Unknown error";
            break;
    }

    errMsg += "\n  file: " + std::string(file) + ", line: " + std::to_string(line);
    throw std::runtime_error(errMsg);
}
