#include <fstream>
#include <iostream>


#include "shaderprogram.hpp"


ShaderProgram::ShaderProgram(const std::unordered_map<GLenum, std::string> &inputShaders, bool validate) :
        enableValidation(validate), shaderSources(inputShaders)
{
    Compile();
    Link();
    StartUseShader();
}

void ShaderProgram::Compile()
{
    shaderProgram = glCreateProgram();
    GL_CHECK_ERRORS();

    if (shaderSources.find(GL_VERTEX_SHADER) != shaderSources.end())
    {
        shaderObjects[GL_VERTEX_SHADER] = LoadShaderObject(GL_VERTEX_SHADER, shaderSources.at(GL_VERTEX_SHADER));
        glAttachShader(shaderProgram, shaderObjects[GL_VERTEX_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderSources.find(GL_FRAGMENT_SHADER) != shaderSources.end())
    {
        shaderObjects[GL_FRAGMENT_SHADER] = LoadShaderObject(GL_FRAGMENT_SHADER, shaderSources.at(GL_FRAGMENT_SHADER));
        glAttachShader(shaderProgram, shaderObjects[GL_FRAGMENT_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderSources.find(GL_GEOMETRY_SHADER) != shaderSources.end())
    {
        shaderObjects[GL_GEOMETRY_SHADER] = LoadShaderObject(GL_GEOMETRY_SHADER, shaderSources.at(GL_GEOMETRY_SHADER));
        glAttachShader(shaderProgram, shaderObjects[GL_GEOMETRY_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderSources.find(GL_TESS_CONTROL_SHADER) != shaderSources.end())
    {
        shaderObjects[GL_TESS_CONTROL_SHADER] = LoadShaderObject(
                GL_TESS_CONTROL_SHADER,
                shaderSources.at(GL_TESS_CONTROL_SHADER)
        );
        glAttachShader(shaderProgram, shaderObjects[GL_TESS_CONTROL_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderSources.find(GL_TESS_EVALUATION_SHADER) != shaderSources.end())
    {
        shaderObjects[GL_TESS_EVALUATION_SHADER] = LoadShaderObject(
                GL_TESS_EVALUATION_SHADER,
                shaderSources.at(GL_TESS_EVALUATION_SHADER)
        );
        glAttachShader(shaderProgram, shaderObjects[GL_TESS_EVALUATION_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderSources.find(GL_COMPUTE_SHADER) != shaderSources.end())
    {
        shaderObjects[GL_COMPUTE_SHADER] = LoadShaderObject(GL_COMPUTE_SHADER, shaderSources.at(GL_COMPUTE_SHADER));
        glAttachShader(shaderProgram, shaderObjects[GL_COMPUTE_SHADER]);
        GL_CHECK_ERRORS();
    }
}


void ShaderProgram::Link()
{
    glLinkProgram(shaderProgram);
    GL_CHECK_ERRORS();

    GLint linkStatus;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linkStatus);
    GL_CHECK_ERRORS();

    GLint linkLogLength;
    glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &linkLogLength);
    GL_CHECK_ERRORS();

    GLchar linkLog[linkLogLength];
    glGetProgramInfoLog(shaderProgram, linkLogLength, nullptr, linkLog);
    GL_CHECK_ERRORS();

    if (linkStatus != GL_TRUE)
    {
        shaderProgram = 0;
        throw std::runtime_error("Shader program linking failed.\n" + std::string(linkLog));
    }
    else if (linkLogLength) { std::cerr << linkLog << std::endl; }

    if (enableValidation)
    {
        glValidateProgram(shaderProgram);
        GL_CHECK_ERRORS();

        GLint validStatus;
        glGetProgramiv(shaderProgram, GL_VALIDATE_STATUS, &validStatus);
        GL_CHECK_ERRORS();

        GLint validLogLength;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &validLogLength);
        GL_CHECK_ERRORS();

        GLchar validLog[validLogLength];
        glGetProgramInfoLog(shaderProgram, validLogLength, nullptr, validLog);
        GL_CHECK_ERRORS();

        if (validStatus != GL_TRUE)
        {
            shaderProgram = 0;
            throw std::runtime_error("Shader program validation failed.\n" + std::string(validLog));
        }
        else if (validLogLength) { std::cerr << validLog << std::endl; }
    }
}


void ShaderProgram::Release()
{
    StopUseShader();
    if (shaderObjects.find(GL_VERTEX_SHADER) != shaderObjects.end())
    {
        glDetachShader(shaderProgram, shaderObjects[GL_VERTEX_SHADER]);
        GL_CHECK_ERRORS();
        glDeleteShader(shaderObjects[GL_VERTEX_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderObjects.find(GL_FRAGMENT_SHADER) != shaderObjects.end())
    {
        glDetachShader(shaderProgram, shaderObjects[GL_FRAGMENT_SHADER]);
        GL_CHECK_ERRORS();
        glDeleteShader(shaderObjects[GL_FRAGMENT_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderObjects.find(GL_GEOMETRY_SHADER) != shaderObjects.end())
    {
        glDetachShader(shaderProgram, shaderObjects[GL_GEOMETRY_SHADER]);
        GL_CHECK_ERRORS();
        glDeleteShader(shaderObjects[GL_GEOMETRY_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderObjects.find(GL_TESS_CONTROL_SHADER) != shaderObjects.end())
    {
        glDetachShader(shaderProgram, shaderObjects[GL_TESS_CONTROL_SHADER]);
        GL_CHECK_ERRORS();
        glDeleteShader(shaderObjects[GL_TESS_CONTROL_SHADER]);
        GL_CHECK_ERRORS();
    }

    if (shaderObjects.find(GL_TESS_EVALUATION_SHADER) != shaderObjects.end())
    {
        glDetachShader(shaderProgram, shaderObjects[GL_TESS_EVALUATION_SHADER]);
        GL_CHECK_ERRORS();
        glDeleteShader(shaderObjects[GL_TESS_EVALUATION_SHADER]);
        GL_CHECK_ERRORS();
    }
    if (shaderObjects.find(GL_COMPUTE_SHADER) != shaderObjects.end())
    {
        glDetachShader(shaderProgram, shaderObjects[GL_COMPUTE_SHADER]);
        GL_CHECK_ERRORS();
        glDeleteShader(shaderObjects[GL_COMPUTE_SHADER]);
        GL_CHECK_ERRORS();
    }

    glDeleteProgram(shaderProgram);
    GL_CHECK_ERRORS();
}

void ShaderProgram::Reload()
{
    StopUseShader();
    Release();
    Compile();
    Link();
    StartUseShader();
}

GLuint ShaderProgram::LoadShaderObject(GLenum type, const std::string &filename)
{
    std::ifstream fs(filename);
    if (!fs.is_open()) { throw std::runtime_error("ERROR: Could not read shader from " + filename); }

    std::string headerText;
    std::getline(fs, headerText);
    headerText += '\n';

    for (const auto &element : defines)
    {
        headerText += "#define " + element.first + ' ';
        headerText += std::to_string(element.second);
        headerText += '\n';
    }

    std::string shaderText{std::istreambuf_iterator<char>(fs), {}};

    GLuint newShaderObject = glCreateShader(type);
    GL_CHECK_ERRORS();

    const char *shaderSrc[] =
            {
                    headerText.c_str(),
                    shaderText.c_str()
            };
    glShaderSource(newShaderObject, 2, shaderSrc, nullptr);
    GL_CHECK_ERRORS();

    glCompileShader(newShaderObject);
    GL_CHECK_ERRORS();

    GLint compileStatus;
    glGetShaderiv(newShaderObject, GL_COMPILE_STATUS, &compileStatus);
    GL_CHECK_ERRORS();

    GLint logLength;
    glGetShaderiv(newShaderObject, GL_INFO_LOG_LENGTH, &logLength);
    GL_CHECK_ERRORS();

    GLchar log[logLength];
    glGetShaderInfoLog(newShaderObject, logLength, nullptr, log);
    GL_CHECK_ERRORS();

    if (compileStatus != GL_TRUE)
    {
        throw std::runtime_error("Shader " + filename + " compilation failed.\n" + std::string(log));
    }
    else if (logLength) { std::cerr << log << std::endl; }

    return newShaderObject;
}

void ShaderProgram::StartUseShader() const
{
    glUseProgram(shaderProgram);
    GL_CHECK_ERRORS();
}

void ShaderProgram::StopUseShader() const
{
    glUseProgram(0);
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetDefine(const std::string &name, int value)
{
    if (value <= 0) { defines.erase(name); }
    else { defines[name] = value; }
}

void ShaderProgram::ResetDefines()
{ defines.clear(); }

void ShaderProgram::SetUniform(const std::string &location, int value) const
{
    glUniform1i(GetUniformLocation(location), value);
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetUniform(const std::string &location, unsigned int value) const
{
    glUniform1ui(GetUniformLocation(location), value);
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetUniform(const std::string &location, float value) const
{
    glUniform1f(GetUniformLocation(location), value);
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetUniform(const std::string &location, double value) const
{
    glUniform1d(GetUniformLocation(location), value);
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetUniform(const std::string &location, LiteMath::float4x4 a_mat) const
{
    glUniformMatrix4fv(GetUniformLocation(location), 1, GL_TRUE, a_mat.L());
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetUniform(const std::string &location, LiteMath::int2 a_vec) const
{
    glUniform2iv(GetUniformLocation(location), 1, a_vec.L());
    GL_CHECK_ERRORS();
}

void ShaderProgram::SetUniform(const std::string &location, LiteMath::float3 a_vec) const
{
    glUniform3fv(GetUniformLocation(location), 1, a_vec.L());
    GL_CHECK_ERRORS();
}

GLint ShaderProgram::GetUniformLocation(const std::string &location) const
{
    GLint uniformLocation = glGetUniformLocation(shaderProgram, location.c_str());
    GL_CHECK_ERRORS();
    if (uniformLocation == -1) { throw std::runtime_error("Uniform  " + location + " not found"); }
    return uniformLocation;
}
