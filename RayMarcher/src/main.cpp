#ifndef GLFW_DLL
#define GLFW_DLL
#endif


#include "common.hpp"
#include "litemath.hpp"
#include "interface.hpp"
#include "shaderprogram.hpp"

#include <iostream>
#include <sstream>

#include <GLFW/glfw3.h>
#include <cstring>


// Translation and rotation of the camera
LiteMath::float3 T;
LiteMath::float3x3 R;

// Mouse location and unprocessed movement
LiteMath::float2 mousePosition;
LiteMath::float2 mouseRotation;

// Unprocessed keyboard movement and rotation
LiteMath::float3 keyboardRotation;
LiteMath::float3 keyboardMovement;

// Window size and settings
LiteMath::int2 windowSize;
bool captureMouse = true;
float mouseLookSensitivity = -0.00125f; // Positive for "drag", Negative for "follow".
float keyboardLookSensitivity = 0.0125f;
float keyboardMoveSensitivity = 0.125f;

// Shader options setup
ShaderProgram *currentProgram;
bool softShadows = false;
bool ambientOcclusion = false;
int refractionReflectionDepth = 0;

void resetMouseOrigin(GLFWwindow *window)
{
    // The starting mouse location is the new 'center'.
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    mousePosition = {float(xpos), float(ypos)};
    mouseRotation = {0, 0};
};

// Callbacks
void windowResizeCallback(GLFWwindow *window, int width, int height)
{
    windowSize = {width, height};
    glViewport(0, 0, width, height);
}

void cursorEnterCallback(GLFWwindow *window, int enter)
{
    if (enter) { resetMouseOrigin(window); }
};

void mouseMovementCallback(GLFWwindow *window, double xpos, double ypos)
{
    // Just record mouse movement, actual rotation is performed in tick
    LiteMath::float2 newPos = {float(xpos), float(ypos)};
    if (captureMouse)
    {
        const LiteMath::float2 diff = (newPos - mousePosition);
        mouseRotation.x += diff.y;
        mouseRotation.y += diff.x;
    }
    mousePosition = newPos;
}

void keyEventCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    // Just record keyboard movement, actual rotation and translation is performed in tick
    float sign = 0;
    if (action == GLFW_PRESS) { sign = 1; }
    else if (action == GLFW_RELEASE) { sign = -1; }

    switch (key)
    {
        // Movement
        case GLFW_KEY_A: sign *= -1;
        case GLFW_KEY_D:
        {
            keyboardMovement.x += sign;
            break;
        }
        case GLFW_KEY_F: sign *= -1;
        case GLFW_KEY_R:
        {
            keyboardMovement.y += sign;
            break;
        }
        case GLFW_KEY_W: sign *= -1;
        case GLFW_KEY_S:
        {
            keyboardMovement.z += sign;
            break;
        }
        case GLFW_KEY_H: sign *= -1;
        case GLFW_KEY_Y:
        {
            keyboardRotation.x += sign;
            break;
        }
        case GLFW_KEY_J: sign *= -1;
        case GLFW_KEY_G:
        {
            keyboardRotation.y += sign;
            break;
        }
        case GLFW_KEY_E: sign *= -1;
        case GLFW_KEY_Q:
        {
            keyboardRotation.z += sign;
            break;
        }
            // Increase/Decrease movement speed
        case GLFW_KEY_LEFT_SHIFT:
        case GLFW_KEY_RIGHT_SHIFT:
        {
            if (action == GLFW_PRESS) { keyboardMoveSensitivity *= 2; }
            if (action == GLFW_RELEASE) { keyboardMoveSensitivity /= 2; }
            break;
        }
        case GLFW_KEY_MINUS:
        {
            if (action == GLFW_PRESS) { keyboardMoveSensitivity *= 0.5; }
            break;
        }
        case GLFW_KEY_EQUAL: // PLUS
        {
            if (action == GLFW_PRESS) { keyboardMoveSensitivity *= 2; }
            break;
        }
            // Shader options
        case GLFW_KEY_0: // Reset shader options
        {
            if (action == GLFW_PRESS)
            {
                softShadows = ambientOcclusion = false;
                refractionReflectionDepth = 0;
                currentProgram->ResetDefines();
                currentProgram->Reload();
                std::cout << "Resetting shader options" << std::endl;
            }
            break;
        }
        case GLFW_KEY_1: // Toggle SS
        {
            if (action == GLFW_PRESS)
            {
                softShadows ^= true;
                if (currentProgram)
                {
                    currentProgram->SetDefine("SS", softShadows);
                    currentProgram->Reload();
                    std::cout << (softShadows ? "Enable" : "Disable") << " soft shadows" << std::endl;
                }
            }
            break;
        }
        case GLFW_KEY_2: // Toggle AO
        {
            if (action == GLFW_PRESS)
            {
                ambientOcclusion ^= true;
                if (currentProgram)
                {
                    currentProgram->SetDefine("AO", ambientOcclusion);
                    currentProgram->Reload();
                    std::cout << (ambientOcclusion ? "Enable" : "Disable") << " ambient occlusion" << std::endl;
                }
            }
            break;
        }
        case GLFW_KEY_3: // Decrease RR depth
        {
            if (action == GLFW_PRESS)
            {
                refractionReflectionDepth--;
                refractionReflectionDepth = std::clamp(refractionReflectionDepth, 0, 6);
                if (currentProgram)
                {
                    currentProgram->SetDefine("RR", refractionReflectionDepth);
                    currentProgram->Reload();
                    std::cout << "Setting refraction reflection depth to " << refractionReflectionDepth << std::endl;
                }
            }
            break;
        }
        case GLFW_KEY_4: // Increase RR depth
        {
            if (action == GLFW_PRESS)
            {
                refractionReflectionDepth++;
                refractionReflectionDepth = std::clamp(refractionReflectionDepth, 0, 6);
                if (currentProgram)
                {
                    currentProgram->SetDefine("RR", refractionReflectionDepth);
                    currentProgram->Reload();
                    std::cout << "Setting refraction reflection depth to " << refractionReflectionDepth << std::endl;
                }
            }
            break;
        }
            // Warp to origin
        case GLFW_KEY_ENTER:
        {
            if (captureMouse) { resetMouseOrigin(window); }
            keyboardMoveSensitivity = 0.125f;
            R = LiteMath::rotate({0, 0, 0});
            T = {0, 0, 0};
            break;
        }

            // Toggle mouse capture on ESC
        case GLFW_KEY_ESCAPE:
        {
            if (action == GLFW_PRESS) { captureMouse ^= true; }
            if (captureMouse) { glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); }
            else { glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL); }
            break;
        }

            // Close application on Ctrl+C or backspace
        case GLFW_KEY_C: if (!(mods & GLFW_MOD_CONTROL)) { break; }
        case GLFW_KEY_BACKSPACE:
        {
            glfwSetWindowShouldClose(window, true);
            break;
        }
        default: break;
    }
}

void tick(LiteMath::float4x4 &projMatrix)
{
    R = R * LiteMath::rotate(keyboardLookSensitivity * keyboardRotation) *
        LiteMath::rotate(mouseLookSensitivity * LiteMath::to_float3(mouseRotation, 0));
    T += LiteMath::to_float3x3(projMatrix) * (keyboardMoveSensitivity * LiteMath::normalize0(keyboardMovement));

    mouseRotation = {0, 0};
    projMatrix = LiteMath::translate(T) * R;
};

int main(int argc, char **argv)
{
    int MSAA = 0;
    if (argc == 2)
    { MSAA = std::stoi(argv[1], nullptr, 10); }
    else if (argc != 1)
    {
        std::cout << "Usage:" << std::endl;
        std::cout << "\t" << argv[0] << " [MSAA level]" << std::endl;
    }
    const char *title = "OpenGL ray tracing";
    GLFWwindow *window = init(960, 540, title, 4, 6, MSAA);
    registerCallbacks(window, cursorEnterCallback, windowResizeCallback, mouseMovementCallback, keyEventCallback);

    std::cout << getVendorString() << std::endl;

    //Reset any OpenGL errors which could be present for some reason
    GLenum gl_error;
    do { gl_error = glGetError(); } while (gl_error != GL_NO_ERROR);

    std::unordered_map<GLenum, std::string> shaders;
    shaders[GL_VERTEX_SHADER] = "shaders/vertex.glsl";
    shaders[GL_FRAGMENT_SHADER] = "shaders/fragment.glsl";
    ShaderProgram program(shaders);
    currentProgram = &program;

    glfwSwapInterval(1); // force 60 frames per second

    ObjectData bd[] =
            {
                    // Hollow Box
                    {
                            .modelMatrix=LiteMath::translate({0, 0, 0}),
                            .dimensions={15, 15, 15, 0.1},
                            .color={1, 1, 1, 1},
                            .type={3, 0, 0, 0},
                    },
                    // Boxes
                    {
                            .modelMatrix=LiteMath::rotate_Z(-1.25f) * LiteMath::translate({4.5, 0, 10}),
                            .dimensions={1, 1, 1, 0},
                            .color=LiteMath::HEX2RGB("1f77b4"),
                            .type={0, 0, 0, 0},
                    },
                    {
                            .modelMatrix=LiteMath::rotate_X(-1.25f) * LiteMath::translate({6.5, 0, 10}),
                            .dimensions={1, 1, 1, 0},
                            .color=LiteMath::HEX2RGB("1f77b4"),
                            .type={0, 0, 0, 0},
                    },
                    {
                            .modelMatrix=LiteMath::translate({-5, 5, 10}),
                            .dimensions={2, 2, 1, 0},
                            .color=LiteMath::HEX2RGB("1f77b4"),
                            .type={0, 0, 0, 0},
                    },
                    // Torus
                    {
                            .modelMatrix=LiteMath::translate({0, 5, 10}),
                            .dimensions={1, 1, 0.5, 0.5},
                            .color=LiteMath::HEX2RGB("ff7f0e"),
                            .type={1, 0, 0, 0},
                    },
                    // Sphere
                    {
                            .modelMatrix=LiteMath::translate({5, 5, 10}),
                            .dimensions={.5f, .5f, .5f, 0},
                            .color=LiteMath::HEX2RGB("d62728"),
                            .type={2, 0, 0, 0},
                    },
                    // Center Torus
                    {
                            .modelMatrix=LiteMath::translate({0, 0, 0}),
                            .dimensions={2, 2, .5f, .5f},
                            .color=LiteMath::HEX2RGB("790604"),
                            .type={1, 0, 0, 0},
                    },
                    // Refractive Showcase
                    {
                            .modelMatrix=LiteMath::translate({0, 2.5, 7.5}),
                            .dimensions={1, 1, 1, 0},
                            .color={1.0, 1.0, 1.0, 1.25},
                            .type={0, 1, 1, 0},
                    },
                    // Menger Sponge
                    {
                            .modelMatrix=LiteMath::translate({-.5f, -.5f, 10}),
                            .dimensions={1, 1, 1, 0},
                            .color=LiteMath::HEX2RGB("2ca02c"),
                            .type={5, 0, 0, 0},
                    },
                    // Mandel Bulb
                    {
                            .modelMatrix=LiteMath::translate({-5, 0, 10}),
                            .dimensions={1, 1, 1, 0},
                            .color=LiteMath::HEX2RGB("cbcdcd"),
                            .type={6, 0, 0, 0},
                    },
                    // Reflective Showcase
                    {
                            .modelMatrix=LiteMath::translate({-1, -5, 10}),
                            .dimensions={1, 1, 1, 0},
                            .color={1.0, 1.0, 1.0, 0.25},
                            .type={2, 1, 0, 0},
                    },
                    {
                            .modelMatrix=LiteMath::translate({1, -4, 10}),
                            .dimensions={2, 1, 1, 0},
                            .color=LiteMath::to_float4(LiteMath::HEX2RGB("189ad3"), 0.75),
                            .type={4, 1, 0, 0},
                    },
            };
    LightData pt_lights[] =
            {
                    {
                            .location={0, 5, -5, 0},
                            .color={.75, .25, .25, 20}
                    },
                    {
                            .location={5, 5, 0, 0},
                            .color={.25, .75, .25, 20}
                    },
                    {
                            .location={0, -5, 5, 0},
                            .color={.25, .25, .75, 20}
                    },
            };
    float screeenQuad[][2] =
            {
                    {-.5f, .5f},
                    {-.5f, -.5f},
                    {.5f,  .5f},
                    {.5f,  -.5f}
            };
    GLuint g_vertexBufferObject;
    GLuint g_vertexArrayObject;
    std::tie(g_vertexBufferObject, g_vertexArrayObject) = makeVBOVAO(0, screeenQuad, sizeof(screeenQuad), sizeof(*screeenQuad) / sizeof(float));

    GLuint g_sceneBuffer = makeSSBO(0);
    auto *obj = static_cast<ObjectData *>(makeBuffer(sizeof(bd)));
    std::memcpy(obj, bd, sizeof(bd));

    GLuint g_lightBuffer = makeSSBO(1);
    auto *lights = static_cast<LightData *>(makeBuffer(sizeof(pt_lights)));
    std::memcpy(lights, pt_lights, sizeof(pt_lights));

    std::function<double(void)> fpsCallback = getFPScallback(window, title);

    LiteMath::float4x4 viewMatrix;

    glfwSetCursorPos(window, 0, 0);
    program.StartUseShader();

    while (!glfwWindowShouldClose(window))
    {
        clear();
        fpsCallback();
        glfwPollEvents();
        tick(viewMatrix);

        for (int i = 3; i <= 7; ++i) { obj[i].modelMatrix = LiteMath::rotate_Y(1.0f / 100) * obj[i].modelMatrix; }

        program.SetUniform("U_viewMatrix", viewMatrix);
        program.SetUniform("U_windowSize", windowSize);
        draw(window, 4);
    }

    program.StopUseShader();
    program.Release();

    glDeleteVertexArrays(1, &g_vertexArrayObject);
    GL_CHECK_ERRORS();
    glDeleteBuffers(1, &g_vertexBufferObject);
    GL_CHECK_ERRORS();
    glDeleteBuffers(sizeof(bd), &g_sceneBuffer);
    GL_CHECK_ERRORS();
    glDeleteBuffers(sizeof(pt_lights), &g_lightBuffer);
    GL_CHECK_ERRORS();

    glfwTerminate();
    return 0;
}
