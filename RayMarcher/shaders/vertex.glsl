#version 460

layout(row_major) uniform;
layout(location = 0) in vec2 vertex;

uniform ivec2 U_windowSize;
uniform mat4 U_viewMatrix;

const float c_fov = 3.141592654f/1.5f;

out vec3 rayDirection;
out vec3 rayOrigin;

void main(void)
{
    rayDirection.xy = U_windowSize * vertex + 0.5f;
    rayDirection.z = -U_windowSize.x / tan(c_fov/2.0f);

    rayOrigin = U_viewMatrix[3].xyz;
    rayDirection = mat3(U_viewMatrix) * rayDirection;

    gl_Position = vec4(2*vertex, 0.0, 1.0);
}
