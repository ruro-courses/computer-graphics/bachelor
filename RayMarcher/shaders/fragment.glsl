#version 460

layout(row_major) uniform;

struct ObjectData
{
    mat4x4 modelMatrix;
    vec4 dimensions;
    vec4 color;
    ivec4 type;
};

struct LightData
{
    vec4 location;
    vec4 color;
};


in vec3 rayDirection;
in vec3 rayOrigin;

layout(location = 0) out vec4 fragColor;

layout(std430, row_major, binding = 0) buffer g_sceneBuffer { ObjectData O[]; };
layout(std430, row_major, binding = 1) buffer g_lightBuffer { LightData L[]; };

const int c_maxMarchIters = 1000;
const float c_maxMarchDistance = 1e3f;
const float c_maxPrecision = 1e-6f;
const float c_basePrecision = 1e-3f;

float g_precision = c_maxPrecision;

const float c_diffuse = 1.0;
const float c_specular = 1.0;
const float c_shadow_softness = 64;

const vec3 c_ambiColor = vec3(0.5, 0.5, 0.5);
const vec3 c_bckgColor = vec3(1, 0, 0);

// region Distance Functions

vec3 ToLocal(in vec3 p, in int objId)
{
    return (O[objId].modelMatrix * vec4(p, 1)).xyz;
}

vec3 ToLocalRotation(in vec3 p, in int objId)
{
    return mat3(O[objId].modelMatrix) * p;
}

float TorusDistance(in vec3 p, in int objId)
{
    vec2 proj = vec2(length(p.xy) - O[objId].dimensions.x, p.z);
    return length(proj) - O[objId].dimensions.z;
}

float BoxDistance(in vec3 p, in int objId)
{
    vec3 dist = abs(p) - O[objId].dimensions.xyz;
    return length(max(dist, 0)) + min(max(dist.x, max(dist.y, dist.z)), 0.0);
}

float SphereDistance(in vec3 p, in int objId)
{
    return length(p) - O[objId].dimensions.x;
}

float HollowBoxDistance(in vec3 p, in int objId)
{
    return abs(BoxDistance(p, objId)) - O[objId].dimensions.w;
}

float CrossDistance(in vec3 p, in int objId)
{
    vec3 d = abs(p) - O[objId].dimensions.xyz;
    return
        min(
        min(max(d.y, d.z),
            max(d.x, d.z)),
            max(d.x, d.y)
        );
}

float MengerDistance(in vec3 p, in int objId)
{
    float d = BoxDistance(p, objId);

    float s = 1;
    do
    {
        s *= 3;
        vec3 a = 1 - abs(mod(s * p, 6) - 3);
        d = max(d, CrossDistance(a, objId) / s);
    }
    while (s*g_precision < 1);

    return d;
}

float MandelDistance(in vec3 p, in int objId)
{
    vec3 w = p;
    float m = dot(w, w);

    float dz = 1.0;
    int i = 0;
    do
    {
        float m2 = m*m;
        float m4 = m2*m2;
        dz = 8.0*sqrt(m4*m2*m)*dz + 1.0;

        float x = w.x; float x2 = x*x; float x4 = x2*x2;
        float y = w.y; float y2 = y*y; float y4 = y2*y2;
        float z = w.z; float z2 = z*z; float z4 = z2*z2;

        float k3 = x2 + z2;
        float k2 = inversesqrt(k3 * k3 * k3 * k3 * k3 * k3 * k3);
        float k1 = x4 + y4 + z4 - 6.0*y2*z2 - 6.0*x2*y2 + 2.0*z2*x2;
        float k4 = x2 - y2 + z2;

        w.x = p.x +  64.0*x*y*z * (x2-z2) * k4 * (x4 - 6.0*x2*z2 + z4)*k1*k2;
        w.y = p.y + -16.0*y2*k3*k4*k4 + k1*k1;
        w.z = p.z +  -8.0*y*k4*(x4*x4 - 28.0*x4*x2*z2 + 70.0*x4*z4 - 28.0*x2*z2*z4 + z4*z4)*k1*k2;

        m = dot(w, w);
        i++;
    }
    while (i*sqrt(g_precision) < 1 && m < c_maxMarchDistance);

    return 0.25*log(m)*sqrt(m)/dz;

}

float CapsuleDistance(in vec3 p, in int objId)
{
    p.y -= clamp(p.y, 0.0, O[objId].dimensions.x);
    return length(p) - O[objId].dimensions.y;
}

float Distance(in vec3 p, in int objId)
{
    switch (O[objId].type.x)
    {
        case 0: return BoxDistance(p, objId);
        case 1: return TorusDistance(p, objId);
        case 2: return SphereDistance(p, objId);
        case 3: return HollowBoxDistance(p, objId);
        case 4: return CapsuleDistance(p, objId);
        case 5: return MengerDistance(p, objId);
        case 6: return MandelDistance(p, objId);
        default: break;
    }
    return 0;
}

vec3 Normal(in vec3 p, in int objId)
{
    const vec3 dx = ToLocalRotation(vec3(g_precision, 0, 0), objId);
    const vec3 dy = ToLocalRotation(vec3(0, g_precision, 0), objId);
    const vec3 dz = ToLocalRotation(vec3(0, 0, g_precision), objId);
    const vec3 pl = ToLocal(p, objId);
    vec3 dddxdydz = vec3(
            Distance(pl + dx, objId) - Distance(pl - dx, objId),
            Distance(pl + dy, objId) - Distance(pl - dy, objId),
            Distance(pl + dz, objId) - Distance(pl - dz, objId)
        );
    return normalize(dddxdydz);
}

// endregion

// region Marching Functions

int March(inout vec3 ray_o, in vec3 ray_d)
{
    float d;
    float t = 0;
    int objId = -1;
    int iter = 0;
    g_precision = c_maxPrecision;
    do
    {
        d = c_maxMarchDistance;
        for (int oi = 0; oi < O.length(); ++oi)
        {
            float dnew = Distance(ToLocal(ray_o + ray_d * t, oi), oi);
            if (dnew < d) { objId = oi; d = dnew; }
        }
        t += d;
        iter++;
        g_precision = max(c_maxPrecision, c_basePrecision*t);
        if (d <= g_precision)
        {
            ray_o += ray_d * t;
            return objId;
        }
    }
    while (t < c_maxMarchDistance && iter < c_maxMarchIters);
    return -1;
}

int ShadowMarch(inout vec3 ray_o, in vec3 ray_d, in int masked, in float tlight, out float shadow)
{
    float d = c_maxMarchDistance;
    float t = 0;
    int objId = -1;
    int iter = 0;
    shadow = 1.0;

    #ifdef SS   // --- Shadow Init ---
    float dcurr = c_maxMarchDistance;
    float dlast = 0;
    float dshadow = 1.0;
    #endif      // -------------------

    g_precision = c_maxPrecision;
    do
    {
        d = c_maxMarchDistance;
        for (int oi = 0; oi < O.length(); ++oi)
        {
            float dnew = Distance(ToLocal(ray_o + ray_d * t, oi), oi);
            if (dnew < d) { objId = oi; d = dnew; }

            #ifdef SS   // --- Shadow Distance ---
            if (oi != masked && dnew < dcurr) { dcurr = dnew; }
            #endif      // -----------------------
        }

        #ifdef SS   // --- Shadow Occlusion ---
        float dd = dcurr * dcurr;
        float yy = dd / (2.0 * dlast);
        dd = sqrt(dd - yy * yy);
        dshadow = min(dshadow, c_shadow_softness * dd / max(0, (tlight - t) - yy));
        dlast = dcurr;
        dcurr = c_maxMarchDistance;
        #endif      // ------------------------

        t += d;
        iter++;
        g_precision = max(c_maxPrecision, c_basePrecision*t);
        if (d <= g_precision)
        {
            ray_o += ray_d * t;
            #ifdef SS
            shadow = dshadow;
            #endif
            return objId;
        }
    }
    while (d < c_maxMarchDistance && iter < c_maxMarchIters);
    return -1;
}

#ifdef RR
int RefractMarch(inout vec3 ray_o, in vec3 ray_d, in int objId)
{
    float d;
    float t = 0;
    int iter = 0;

    g_precision = c_maxPrecision;
    do
    {
        d = -Distance(ToLocal(ray_o + ray_d * t, objId), objId);
        t += d;
        iter++;
        g_precision = max(c_maxPrecision, c_basePrecision*t);
        if (d <= g_precision)
        {
            ray_o += ray_d * t;
            return objId;
        }
    }
    while (t < c_maxMarchDistance && iter < c_maxMarchIters);
    return -1;
}
#endif

// endregion

// region Lighting
// Computes Phong illumination and Fresnel coefficients for refraction

#ifdef AO
float AmbientOcclusion(in vec3 Phit, in vec3 Ndir)
{
    const int AO_ITERS = 5;
    const float AO_SCALE = 500 * c_basePrecision/AO_ITERS;
    float ao = 0.0;
    float decay = 1;
    for (int s = 1; s <= AO_ITERS; ++s)
    {
        float d = c_maxMarchDistance;
        for (int oi = 0; oi < O.length(); ++oi)
        {
            d = min(d, Distance(ToLocal(Phit + s*AO_SCALE*Ndir, oi), oi));
        }
        ao += (s*AO_SCALE - d)/decay;
        decay *= 2;
    }
    return max(0, 1 - ao);
}
#endif

vec3 Illuminate(in vec3 Phit, in vec3 Vdir, in vec3 Ndir, in int objId)
{
    #ifdef AO
    vec3 total = c_ambiColor * AmbientOcclusion(Phit, Ndir);
    #else
    vec3 total = c_ambiColor;
    #endif

    for (int li = 0; li < L.length(); ++li)
    {
        const vec3 Lpos = L[li].location.xyz - Phit;
        const vec3 Ldir = normalize(Lpos);
        const float Ldist = dot(Lpos, Lpos);
        const float Lpow = L[li].color.a / Ldist;
        const float dotNL = dot(Ndir, Ldir);
        if (dotNL > 0)
        {
            float shadow = 1;
            vec3 LS = L[li].location.xyz;
            const int occlusion = ShadowMarch(LS, -Ldir, objId, sqrt(Ldist), shadow);
            if (occlusion == objId)
            {
                float Ipow = c_diffuse * dotNL;

                const vec3 Rdir = reflect(Ldir, Ndir);
                const float specular = dot(Vdir, Rdir);
                if (specular > 0)
                { Ipow += c_specular * (specular * specular * specular * specular); }

                total += (Ipow * shadow * Lpow) * L[li].color.rgb;
            }
        }
    }

    return clamp(O[objId].color.rgb * total, 0, 1);
}

#ifdef RR
float Fresnel(in vec3 Vdir, in vec3 Ndir, in vec3 Rdir, in float eta)
{
    const float dotI = dot(-Vdir, Ndir);
    const float dotT = dot(Rdir, -Ndir);
    const float Rs = (dotI-eta*dotT)/(dotI+eta*dotT);
    const float Rp = (dotT-eta*dotI)/(dotT+eta*dotI);
    return 0.5 * (Rs*Rs + Rp*Rp);
}
#endif

// endregion

// region Samplers
// Samplers iteratively ray march and aggregate colors

vec3 Sample0(in vec3 Vpos, in vec3 Vdir, in int innerObjId)
{
    int objId;
    vec3 Phit = Vpos;

    if ((objId = March(Phit, Vdir)) != -1 && innerObjId == -1)
    {
        const vec3 Ndir = Normal(Phit, objId);
        return Illuminate(Phit, Vdir, Ndir, objId);
    }
    else
    { return c_bckgColor; }
}

#ifdef RR
#define RecSample(I, J)                                                                         \
vec3 Sample##I(in vec3 Vpos, in vec3 Vdir, in int innerObjId)                                   \
{                                                                                               \
    int objId;                                                                                  \
    bool inner = innerObjId != -1;                                                              \
    vec3 Phit = Vpos;                                                                           \
    /* Inner samples are taken after a refraction happened and we are inside some object */     \
    if (inner)                                                                                  \
    { objId = RefractMarch(Phit, Vdir, innerObjId); }                                           \
    else                                                                                        \
    { objId = March(Phit, Vdir); }                                                              \
    const float p_precision = g_precision;                                                      \
                                                                                                \
    if (objId != -1)                                                                            \
    {                                                                                           \
        /* Solid colors */                                                                      \
        vec3 Ndir = Normal(Phit, objId);                                                        \
        Ndir = inner ? -Ndir : Ndir;                                                            \
        const vec3 icolor = Illuminate(Phit, Vdir, Ndir, objId);                                \
        if (O[objId].type.y != 0)                                                               \
        {                                                                                       \
            const vec3 rcolor = Sample##J(                                                      \
                    Phit + Ndir*p_precision,                                                    \
                    reflect(Vdir, Ndir),                                                        \
                    innerObjId                                                                  \
                );                                                                              \
            if (O[objId].type.z != 0)                                                           \
            {                                                                                   \
                /* Reflection + Refraction (O.color.a is refraction coefficient) */             \
                const float eta = inner ? 1/O[objId].color.a : O[objId].color.a;                \
                const vec3 Rdir = refract(Vdir, Ndir, eta);                                     \
                if (Rdir == 0) { return rcolor; }                                               \
                const vec3 tcolor = Sample##J(                                                  \
                        Phit - 2*Ndir*p_precision,                                              \
                        Rdir,                                                                   \
                        inner ? -1 : objId                                                      \
                    );                                                                          \
                                                                                                \
                /* Don't use inner rays on last iteration, Phong model doesn't use inner rays*/ \
                if (J==0)                                                                       \
                { return inner ? tcolor : rcolor; }                                             \
                else                                                                            \
                { return mix(tcolor, rcolor, Fresnel(Vdir, Ndir, Rdir, eta)); }                 \
            }                                                                                   \
            else                                                                                \
            {                                                                                   \
                /* Pure Reflections (O.color.a is reflectivity) */                              \
                const float Rc = O[objId].color.a;                                              \
                return mix(icolor, rcolor, Rc);                                                 \
            }                                                                                   \
        }                                                                                       \
        else                                                                                    \
        { return icolor; }                                                                      \
    }                                                                                           \
    else                                                                                        \
    { return c_bckgColor; }                                                                     \
}
#endif

// Generate limited depth recursion via macros

#ifndef RR
#define Sample(o, d, i) Sample0(o, d, i)

#elif RR == 0
#define Sample(o, d, i) Sample0(o, d, i)

#elif RR > 6
#error Refraction and Reflection levels 7 and up are not available

#elif RR >= 0

    #if RR > 0
    RecSample(1, 0)
    #endif
    #if RR > 1
    RecSample(2, 1)
    #endif
    #if RR > 2
    RecSample(3, 2)
    #endif
    #if RR > 3
    RecSample(4, 3)
    #endif
    #if RR > 4
    RecSample(5, 4)
    #endif
    #if RR > 5
    RecSample(6, 5)
    #endif

    #if RR == 1
    #define Sample(o, d, i) Sample1(o, d, i)
    #elif RR == 2
    #define Sample(o, d, i) Sample2(o, d, i)
    #elif RR == 3
    #define Sample(o, d, i) Sample3(o, d, i)
    #elif RR == 4
    #define Sample(o, d, i) Sample4(o, d, i)
    #elif RR == 5
    #define Sample(o, d, i) Sample5(o, d, i)
    #elif RR == 6
    #define Sample(o, d, i) Sample6(o, d, i)
    #endif

#else
#error Invalid Refraction and Reflection level
#endif

// endregion

void main(void)
{
    fragColor.rgb = Sample(rayOrigin, normalize(rayDirection), -1);
    fragColor.a = 1;
}


