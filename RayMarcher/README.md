# Signed Distance Function Ray Marching in OpenGL

## Features:

|                              Feature                              | Notes / How to enable                                                                               |            Points            |
|:-----------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------|:----------------------------:|
|                       At least 3 primitives                       | Cube, Sphere, Torus, Menger Sponge, Mendelbulb, Hollow Box and Capsule                              |             ---              |
|                       At least 3 materials                        | Different colors, reflective, refractive materials (also see Reflection&Refraction)                 |             ---              |
|                         At least 2 lights                         | Scene has 3 tinted light sources                                                                    |             ---              |
|                           Sharp shadows                           | ---                                                                                                 |              +1              |
|                           Soft shadows                            | Press `1` to toggle soft shadows                                                                    |             +2-3             |
|                       Reflection&Refraction                       | Press `4` to increase maximum recursion depth for reflection/refraction (see Reflection&Refraction) | Reflection+1, Refraction+2-3 |
|                           Anti-aliasing                           | 16xMSAA can be enabled by starting the application with `./main 16` argument                        |              +1              |
|                         Ambient occlusion                         | Press `2` to toggle ambient occlusion                                                               |             +2-4             |
|                         Fractal surfaces                          | The scene contains the Menger Sponge and Mandelbulb fractals (see Fractals)                         |             +2-6             |
| The scene is realistic and contains various objects and materials | 🤔                                                                                                  |             +1-2             |
|                    Constructive solid geometry                    | The Menger Sponge and the Hollow Box (you start inside the hollow box) use CSG                      |             +1-2             |
|                    Movement with mouse + WASD                     | See keybindings                                                                                     |              +2              |

#### Extra features:

- FPS counter in title bar.
- Arbitrary precision ray marching based on pixel distance. (see
  Fractals)
- SSBO and Buffer Mapping allows to seamlessly change the number and
  properties of objects and lights from C++ code.
- Modified ShaderProgram allows adding GLSL `#define` statements from
  C++ code and automatic recompilation.

#### Refraction & Reflection:

The scene contains a sphere+capsule pair (look up from spawn) with
metallic reflectivity and a rotating cube (look down from spawn) with
glass refraction. Physically correct refraction also implies reflection.
The mixing ratios for reflected and refracted light are computed
according to the
[Fresnel equations](en.wikipedia.org/wiki/Fresnel_equations).

The GPU doesn't support **real** recursion, so it is emulated through
tons of macros. This means that to change the max recursion depth, the
shaders must be recompiled. Thus, high R&R levels may take more than 30
seconds to render the first frame on slow computers.

To increase the recusrsion level press `4`, to decrease the recusrion
level press `3` (or `0` to reset every shader setting).

#### Fractals:

The recursion depth (detail level) of the fractal is automatically
adjusted based on pixel proximity to camera. This allows fast render
times for fractals, while making the fractal look "infinite". To explore
the small details of the fractal, use `+`/`-` keys to adjust movement
speed. You can 'dive' closer and closer to the fractal surface without
ever reaching it.

## Build and run:

#### Build
Requrements:
- `GCC` (tested on version `8.2.1`, must support `C++17`)
- `OpenGL` (version `4.6` or higher)
- `GLFW` (tested on version `3.2.1-2`)
  > Install via `apt-get install libglfw3`
``` bash
mkdir build
cd build
cmake .. 
make
```

#### Run
Optional `[MSAA]` parameter specifies number of samples used for MSAA,
e.x. `./main 64` enables 64xMSAA. (slow!)
``` bash
./main [MSAA]
```

## Keybindings:

|       Keys       | Actions                                                                          |
|:----------------:|:---------------------------------------------------------------------------------|
|      Mouse       | Rotate camera (free rotation)                                                    |
| CTRL+C/Backspace | Exit                                                                             |
|       ESC        | Toggle mouse capture                                                             |
|   W/A/S/D/R/F    | Move forward/left/backward/right/up/down                                         |
|       Q/E        | Rotate camera counter-clockwise/clockwise                                        |
|     Y/G/H/J      | Rotate camera up/left/down/right (alternative to mouse)                          |
|  LSHIFT/RSHIFT   | Increase movement speed x2 (while held)                                          |
|       +/-        | Increase/decrease movement speed x2 (incremental)                                |
|      ENTER       | Reset camera rotation, position and speed to defaults                            |
|        0         | Reset shader options                                                             |
|        1         | Enable soft shadows                                                              |
|        2         | Enable ambient occlusion                                                         |
|       4/3        | Increase/decrease maximum reflection and refraction depth (minimum 0, maximum 6) |

## Screenshots:

#### Menger Sponge

![Menger Sponge](screen01.jpg)

------------------------------

#### No Anti-aliasing

![No Anti-aliasing](NOAA.jpg)

------------------------------

#### 64xMulti Sample Anti-aliasing

![64xMSAA](64xMSAA.jpg)

------------------------------
